﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System.Drawing.Imaging;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System.Configuration;
using System.Data.SqlServerCe;
using System.Collections.Generic;
using System.Threading;
using OpenQA.Selenium.Interactions;
using System.Linq;
using System.Collections;
//using log4net.Config;
//using log4net;
using OpenQA.Selenium.Support.UI;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;

namespace MaxRav_Selenium
{
    [TestClass]
    public class UnitTest1
    {
        public static string pathStr = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static string pathJenkinsStr = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        public static string currentVerOfMaxRav = "Prod_MaxRav"; // TestProd_MaxRav,Prod_MaxRav,Test_MaxRav
        public static string FullName = ConfigurationManager.AppSettings["FullName"];
        static IWebDriver driver;
        private bool acceptNextAlert = true;
        //public static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // just comment for checking comment

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [AssemblyInitialize]
        //[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config", Watch = true)]
        public static void SetUp(TestContext context)
        {
            string[] strArray = pathStr.Split(new string[] { "automaxrav" }, StringSplitOptions.None);

            var testStr = strArray[0] + @"automaxrav\MaxRav_Selenium\bin\Debug";

            // Switch to Chrome Driver

            // LOCAL RUN
            //driver = new ChromeDriver("C:\\WebDrivers");
            
            // JENKINS RUN
            driver = new ChromeDriver(strArray[0] + @"automaxrav\MaxRav_Selenium\bin\Debug");
            pathJenkinsStr = strArray[0] + @"automaxrav\MaxRav_Selenium\bin\Debug";



            //XmlConfigurator.Configure();
            //var options = new ChromeOptions();
            //options.AddArgument("no-sandbox");
            // Switch to IE
            //driver = new InternetExplorerDriver("C:\\WebDrivers");
        }

        //[TestMethod]
        public void Case0_Login()
        {
            string maxRavVer = currentVerOfMaxRav;

            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav]);
            //driver.Navigate().GoToUrl("https://maxrav-testprod-azure.churchillcap.com");
            string targetText = "This is test site. Version 1.13.0.0";

            if (maxRavVer != "Prod_MaxRav")
            {
                try
                {
                    IWebElement body = driver.FindElement(By.Id("lbTestSiteNotification"));
                    Assert.IsTrue(body.Text.Contains(targetText), "\nFollowing text not found: " + targetText);
                }
                catch (Exception e)
                {
                    takeScreenshot(e.ToString());
                    Assert.Fail("Exception during the login: " + e.ToString());
                }                
            }

            WaitUntilElementIsPresent(driver, By.Id("UserName"));
            ActionFunction("UserName", ConfigurationManager.AppSettings["Login"], driver, "SendKeys");
            ActionFunction("Password", ConfigurationManager.AppSettings["Password"], driver, "SendKeys");
            //ActionFunction("UserName", "EugeneAZ", driver, "SendKeys");
            //ActionFunction("Password", "ChurchillCap_111_EugeneAZ", driver, "SendKeys");
            ActionFunction("LoginButton", "", driver, "Click");
            WaitUntilElementIsPresent(driver, By.TagName("body"));
            if (!IfTextPresentAtPage(driver, ("Welcome " + FullName)))
            {
                takeScreenshot("");
                Assert.Fail("\nThe page is not loaded");
            }
            else
            {
                addLogInformation("Login Successful");
                try
                {
                    Assert.AreEqual("Recent Client Discussions on our Ideas:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_T']/em")).Text);
                    Assert.AreEqual("My Client List:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_myClientList_T']/em")).Text);
                    Assert.AreEqual("Latest Focus Ideas:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_latestFocusIdeas_T']/em")).Text);
                    Assert.AreEqual("Published Notes:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_publishedNotes_T']/em")).Text);
                    Assert.AreEqual("Announcement List:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_announcementList_T']/em")).Text);
                    Assert.AreEqual("Upcoming Company Events:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_companyEvents_T']/em")).Text);
                    Assert.AreEqual("Most Recent Idea News:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_mostRecentIdeaNotes_T']/em")).Text);
                    Assert.AreEqual("Global Call Monitor List:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_globalCall_T']/em")).Text);
                    Assert.AreEqual("Idea Notes", driver.FindElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_T']/em")).Text);
                    Assert.AreEqual("Last Month Top 10 Ideas:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_mostViewedIdeas_T']/em")).Text);
                }
                catch (Exception e)
                {
                    takeScreenshot(e.ToString());
                    Assert.Fail("Exception during the login: " + e.ToString());
                }
                addLogInformation("The Home Page is ok");
            }
        }

        public void CaseLast_Logout()
        {
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav]);
            WaitUntilElementIsPresent(driver, By.TagName("body"));
            if (!IfTextPresentAtPage(driver, ("Welcome " + FullName)))
            {
                takeScreenshot("");
                Assert.Fail("\nCase2_Logout: The page is not loaded.");
            }

            if (IfTextPresentAtPage(driver, ("Welcome " + FullName)))
            {
                ActionFunction("ctl00_logout", "", driver, "Click");
                WaitUntilElementIsPresent(driver, By.Id("UserName"));
                addLogInformation("Sucessfull log out. Done.");
                Console.WriteLine();
            }
        }

        //[TestMethod]
        public void Case01_LoginAndLogout()
        {

            string maxRavVer = currentVerOfMaxRav;
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav]);
            string targetText = "This is test site. Version 1.13.0.0";

            if (maxRavVer != "Prod_MaxRav")
            {
                IWebElement body = driver.FindElement(By.Id("lbTestSiteNotification"));
                Assert.IsTrue(body.Text.Contains(targetText), "\nFollowing text not found: " + targetText);
            }

            currentFormOfChecking = "LOGIN PAGE";
            addLogInformation("Check " + currentFormOfChecking);

            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Login Page", driver.Title);
                Assert.AreEqual("User Name:", driver.FindElement(By.Id("UserNameLabel")).Text);
                Assert.AreEqual("Password:", driver.FindElement(By.Id("PasswordLabel")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("hForget")).Text, "^Forgot your password[\\s\\S]$"));
                Assert.AreEqual("Churchill Capital Ltd. site is a subscription-based website for professional investors only. Please, contact sales@churchillcap.com if you need any further information.", driver.FindElement(By.Id("additionalMessage")).Text);
                Assert.AreEqual("Remember Me   ", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[2]/following::label[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }

            // Wrong password case
            ActionFunction("UserName", ConfigurationManager.AppSettings["Login"], driver, "SendKeys");
            ActionFunction("Password", "wrong password", driver, "SendKeys");
            ActionFunction("LoginButton", "", driver, "Click");
            // Verify additional lines are displayed
            var body_ = driver.FindElement(By.TagName("body"));
            verificationTextOnThePage("Your login attempt was not successful.", body_.Text);
            verificationTextOnThePage("Please try again.", body_.Text);

            // Login with proper credentials
            Case0_Login();

            // Logout
            ClickCurrentElement(By.Id("ctl00_logout"));

            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Login Page", driver.Title);
                Assert.AreEqual("User Name:", driver.FindElement(By.Id("UserNameLabel")).Text);
                Assert.AreEqual("Password:", driver.FindElement(By.Id("PasswordLabel")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("hForget")).Text, "^Forgot your password[\\s\\S]$"));
                Assert.AreEqual("Churchill Capital Ltd. site is a subscription-based website for professional investors only. Please, contact sales@churchillcap.com if you need any further information.", driver.FindElement(By.Id("additionalMessage")).Text);
                Assert.AreEqual("Remember Me   ", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[2]/following::label[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception: " + e.ToString());
            }
            addLogInformation("Sucessfull log out. Done.");
            Console.WriteLine();

        }
        
        [TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_01_HomeTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("HOME TAB");
            currentFormOfChecking = "Client Discussion form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/div/ul/li[1]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Discussion", driver.Title);
                Assert.AreEqual("Date:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr/td")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.Id("ctl00_body_DiscussionDigest_clientSelector_ClientText")).Text);
                Assert.AreEqual("Client Groups:", driver.FindElement(By.Id("ctl00_body_DiscussionDigest_LabelClientsGroup")).Text);
                Assert.AreEqual("Idea:", driver.FindElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_TickerText")).Text);
                Assert.AreEqual("User Name:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[9]/td")).Text);
                Assert.AreEqual("Discussion Type:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[11]/td")).Text);
                Assert.AreEqual("Duration:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[15]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[17]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[19]/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();
 
            currentFormOfChecking = "Idea Note form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[2]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Idea Note", driver.Title);
                Assert.AreEqual("Date:", driver.FindElement(By.XPath("//div[@id='ctl00_body_QuickNote1_up']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Source:", driver.FindElement(By.XPath("//div[@id='ctl00_body_QuickNote1_up']/table/tbody/tr[8]/td")).Text);
                Assert.AreEqual("Rating:", driver.FindElement(By.XPath("//div[@id='ctl00_body_QuickNote1_up']/table/tbody/tr[10]/td")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_TickerText")).Text, "^Idea:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_body_QuickNote1_up']/table/tbody/tr[4]/td")).Text, "^Subject:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_body_QuickNote1_up']/table/tbody/tr[6]/td")).Text, "^Content:[\\s\\S]*$"));
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Company Event form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Company Event", driver.Title);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[7]/td")).Text, "Event:*"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr/td")).Text, "^Date:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[3]/td")).Text, "^Ticker:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/table[2]/tbody/tr[5]/td")).Text, "^Region:[\\s\\S]*$"));

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Client Meeting Notes form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Meeting Notes", driver.Title);
                Assert.AreEqual("Client Meeting Notes", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(driver.PageSource.Contains("Meeting notes:*"));
                Assert.IsTrue(driver.PageSource.Contains("Country"));
                Assert.IsTrue(driver.PageSource.Contains("City"));
                Assert.IsTrue(driver.PageSource.Contains("Duration:"));
                Assert.IsTrue(driver.PageSource.Contains("Clients:*"));
                Assert.IsTrue(driver.PageSource.Contains("Attendees:*"));
                Assert.IsTrue(driver.PageSource.Contains("Name"));
                Assert.IsTrue(driver.PageSource.Contains("Ideas:*"));
                Assert.IsTrue(driver.PageSource.Contains("Meeting notes:*"));

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Multi Idea Discussion form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[5]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[5]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Multi Idea Discussion", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Multi Idea Discussion", driver.Title);
                Assert.AreEqual("Date:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table[2]/tbody/tr/td")).Text);
                Assert.AreEqual("User Name:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table[2]/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Discussion Type:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table[2]/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Idea:", driver.FindElement(By.Id("ctl00_body_ISelector1_TickerText")).Text);
                Assert.AreEqual("Duration:", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[6]/td")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_body_CSelector_ClientText")).Text, "^Client Name:[\\s\\S]*$"));

                List<string> tempList = new List<string>();
                tempList.Add("Duration");
                tempList.Add("Idea");
                tempList.Add(@"Discussion | Public (VISIBLE BY CLIENTS):");
                tempList.Add(@"Discussion | Private (INTERNAL ONLY):");

                int count = 0;
                foreach (var item in tempList)
                {
                    count = countOfTextOnThePage(driver, item);
                    if (count != 6 && item != "Idea")
                    {
                        takeScreenshot("");
                        Assert.Fail("Should be six " + item + "; Actual is " + count + ".");
                        addLogInformation(currentFormOfChecking + "should be reviewed manually");
                    }
                    else if (count != 7 && item == "Idea")
                    {
                        takeScreenshot("");
                        Assert.Fail("Check out the Idea title!");
                        addLogInformation(currentFormOfChecking + " should be reviewed manually");
                    }
                }

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Multi Client Discussion form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[6]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add Multi Client Discussion", driver.Title);
                Assert.AreEqual("Multi Client Discussion", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr/td")).Text, "^Date:[\\s\\S]*$"));
                Assert.AreEqual("Idea:", driver.FindElement(By.Id("ctl00_body_ISelector_TickerText")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr[4]/td")).Text, "^User Name:[\\s\\S]*$"));
                Assert.AreEqual("Discussion Type:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.Id("ctl00_body_ClientSelector1_ClientText")).Text);
                Assert.AreEqual("Duration:", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.Id("ctl00_body_ClientSelector2_ClientText")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Multi Client Idea Discussion form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[7]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Multi Client Idea Discussion", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Multi Client Idea Discussion", driver.Title);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr/td")).Text, "^Date:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr[3]/td")).Text, "^User Name:[\\s\\S]*$"));
                Assert.AreEqual("Discussion Type:", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td/table/tbody/tr[5]/td")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.Id("ctl00_body_ClientSelector1_ClientText")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.Id("ctl00_body_ClientSelector2_ClientText")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_body_ISelector1_TickerText")).Text, "^Idea:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_body_ISelector2_TickerText")).Text, "^Idea:[\\s\\S]*$"));
                Assert.AreEqual("Duration:", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[3]/td")).Text);
                Assert.AreEqual("Duration:", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[3]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[5]/td")).Text);
                Assert.AreEqual("Discussion | Public (VISIBLE BY CLIENTS):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[5]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP2']/table/tbody/tr[7]/td")).Text);
                Assert.AreEqual("Discussion | Private (INTERNAL ONLY):", driver.FindElement(By.XPath("//div[@id='ctl00_body_UP1']/table/tbody/tr[7]/td")).Text);

                List<string> tempList = new List<string>();
                tempList.Add("Duration:");
                tempList.Add("Idea");
                tempList.Add("Client Name:");
                tempList.Add(@"Discussion | Public (VISIBLE BY CLIENTS):");
                tempList.Add(@"Discussion | Private (INTERNAL ONLY):");

                int count = 0;
                foreach (var item in tempList)
                {
                    count = countOfTextOnThePage(driver, item);
                    if (count != 6 && item != "Idea")
                    {
                        takeScreenshot("");
                        Assert.Fail("Should be six " + item + "; Actual is " + count + ".");
                        addLogInformation(currentFormOfChecking + " should be reviewed manually");
                    }
                    else if (count != 7 && item == "Idea")
                    {
                        takeScreenshot("");
                        Assert.Fail("Check out the Idea title!");
                        addLogInformation(currentFormOfChecking + " should be reviewed manually");
                    }
                }

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Recommendation form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[8]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr/td/span")).Text, "^Analyst[\\s\\S]*:$"));
                Assert.AreEqual("Add Recommendation", driver.Title);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[2]/td/span")).Text, "^Company1[\\s\\S]*:$"));
                Assert.AreEqual("Company2:", driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[3]/td/span")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[2]/td[2]/span")).Text, "^Rec[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[3]/td[2]/span")).Text, "^Rec[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[4]/td/span")).Text, "^Stock Ratio[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[4]/td[2]/span")).Text, "^Spread Target[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[2]/td[3]/span")).Text, "^Price Target[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[4]/td[3]/span")).Text, "^Ratio Target[\\s\\S]*:$"));
                Assert.AreEqual("Timestamp:", driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[6]/td/span")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[6]/td[2]/span")).Text, "^Deal File[\\s\\S]*:$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='edit-element-host']/table/tbody/tr[6]/td[3]/span")).Text, "^Time Period[\\s\\S]*:$"));
                Assert.AreEqual("Related Recommendations", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/span")).Text);
                Assert.AreEqual("Timestamp", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[2]")).Text);
                Assert.AreEqual("Tickers", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[3]")).Text);
                Assert.AreEqual("Directions", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[4]")).Text);
                Assert.AreEqual("Period", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[5]")).Text);
                Assert.AreEqual("Stock Ratio", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[6]")).Text);
                Assert.AreEqual("Spread Target", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[7]")).Text);
                Assert.AreEqual("Ratio Target", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[8]")).Text);
                Assert.AreEqual("Price Target", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[9]")).Text);
                Assert.AreEqual("Analyst", driver.FindElement(By.XPath("//div[@id='recommendation-list-host']/table/thead/tr/th[10]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            closeTheForm(mainWeb);
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_02_TheMarketPulseTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            currentFormOfChecking = "Add Entry form";
            addLogInformation("THE MARKET PULSE TAB");
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Use raw HTML editor", driver.FindElement(By.XPath("//table[@id='contentTable']/tbody/tr/td/div/div[2]/label[4]")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.XPath("//table[@id='contentTable']/tbody/tr/td/div/div[2]/label[3]")).Text);
                Assert.AreEqual("Control panel", driver.FindElement(By.XPath("//form[@id='aspnetForm']/table/tbody/tr/td[2]/h1")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.XPath("//table[@id='contentTable']/tbody/tr/td/div/div[2]/label")).Text);
                Assert.AreEqual("Add entry", driver.Title);
                Assert.AreEqual("Path:", driver.FindElement(By.Id("ctl00_cphAdmin_txtContent_TinyMCE1_txtContent_path_row")).Text);
                Assert.AreEqual("Upload image", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr/td")).Text);
                Assert.AreEqual("Upload file", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Slug (optional)", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[3]/td")).Text);
                Assert.AreEqual("Description", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Categories", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[5]/td")).Text);
                Assert.AreEqual("Extract from title", driver.FindElement(By.LinkText("Extract from title")).Text);
                Assert.AreEqual("Tags", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Settings", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[7]/td")).Text);
                Assert.AreEqual("Enable comments", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[7]/td[2]/span/label")).Text);
                Assert.AreEqual("Publish", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[7]/td[2]/label")).Text);
                Assert.AreEqual("Separate each tag with a comma", driver.FindElement(By.XPath("//table[@id='entrySettings']/tbody/tr[6]/td[2]/span")).Text);
                Assert.AreEqual("Make a donation", driver.FindElement(By.LinkText("Make a donation")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            driver.Navigate().Back();

            currentFormOfChecking = "Market Calendar form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Printer Friendly Version:", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Filter:", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Mon", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th")).Text);
                Assert.AreEqual("Tue", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[2]")).Text);
                Assert.AreEqual("Wed", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[3]")).Text);
                Assert.AreEqual("Thu", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[4]")).Text);
                Assert.AreEqual("Fri", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[5]")).Text);
                Assert.AreEqual("Sat", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[6]")).Text);
                Assert.AreEqual("Sun", driver.FindElement(By.XPath("//table[@id='ctl00_MC_Calendar1_c']/tbody/tr[2]/th[7]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Featured News form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[2]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("FEATURED NEWS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Delete Featured News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Featured News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Type", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th")).Text);
                Assert.AreEqual("Enabled", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("First Day (GMT)", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[4]")).Text);
                Assert.AreEqual("Last Day (GMT)", driver.FindElement(By.LinkText("Last Day (GMT)")).Text);
                Assert.AreEqual("News Date", driver.FindElement(By.LinkText("News Date")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[7]")).Text);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        [TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_03_IdeasTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            currentFormOfChecking = "Active Ideas form";
            addLogInformation("IDEAS TAB");
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Active Ideas", driver.Title);
                Assert.AreEqual("Active Ideas", driver.FindElement(By.Id("ctl00_Header_ltTitle")).Text);
                Assert.AreEqual("Focus Ideas:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Assets:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Regions:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Countries:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Strategies:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[13]")).Text);
                Assert.AreEqual("Sub-Industry:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[15]")).Text);
                Assert.AreEqual("N.A.", driver.FindElement(By.XPath("(//a[contains(text(),'N.A.')])[2]")).Text);
                Assert.AreEqual("Ideas", driver.FindElement(By.XPath("(//a[contains(text(),'Ideas')])[2]")).Text);
                Assert.AreEqual("Companies", driver.FindElement(By.LinkText("Companies")).Text);
                Assert.AreEqual("Strategy", driver.FindElement(By.LinkText("Strategy")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Industry", driver.FindElement(By.LinkText("Industry")).Text);
                Assert.AreEqual("Updated", driver.FindElement(By.LinkText("Updated")).Text);
                Assert.AreEqual("Owner", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[7]")).Text);
                Assert.AreEqual("Published", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[8]")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[9]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("clearLink")).Text);
                Assert.AreEqual("Search in Archived Ideas", driver.FindElement(By.Id("linkArchieveIdeasSelector")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            /*
            currentFormOfChecking = "Archive Ideas form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Archive Ideas", driver.Title);
                Assert.AreEqual("Archived Ideas", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Focus Ideas:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Assets:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Regions:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Countries:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Strategies:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[13]")).Text);
                Assert.AreEqual("Sub-Industry:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[15]")).Text);
                Assert.AreEqual("N.A.", driver.FindElement(By.XPath("(//a[contains(text(),'N.A.')])[2]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("clearLink")).Text);
                Assert.AreEqual("Search in Active Ideas", driver.FindElement(By.Id("linkArchieveIdeasSelector")).Text);
                Assert.AreEqual("Ideas", driver.FindElement(By.XPath("(//a[contains(text(),'Ideas')])[2]")).Text);
                Assert.AreEqual("Companies", driver.FindElement(By.LinkText("Companies")).Text);
                Assert.AreEqual("Strategy", driver.FindElement(By.LinkText("Strategy")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Industry", driver.FindElement(By.LinkText("Industry")).Text);
                Assert.AreEqual("Updated", driver.FindElement(By.LinkText("Updated")).Text);
                Assert.AreEqual("Owner", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[7]")).Text);
                Assert.AreEqual("Published", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[8]")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[9]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Create New Idea form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Create new Idea", driver.Title);
                Assert.AreEqual("CREATE NEW IDEA", driver.FindElement(By.Id("ctl00_MC_ltrIdeaCreateHeader")).Text);
                Assert.AreEqual("Deal setup", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Please, select asset type", driver.FindElement(By.XPath("//div[@id='ctl00_MC_ideaEdit_ideaSetup_UP']/table/tbody/tr/td/label")).Text);
                Assert.AreEqual("Change Strategy", driver.FindElement(By.Id("ctl00_MC_ideaEdit_btnStartStrategyChange")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Install BLPupdate form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("BLP Updater", driver.Title);
                Assert.AreEqual("Churchill Capital", driver.FindElement(By.XPath("//span")).Text);
                Assert.AreEqual("BLP Updater", driver.FindElement(By.XPath("//tr[3]/td/span")).Text);
                Assert.AreEqual("Name:", driver.FindElement(By.XPath("//b")).Text);
                Assert.AreEqual("BLP Updater", driver.FindElement(By.XPath("//td[3]")).Text);
                Assert.AreEqual("Version:", driver.FindElement(By.XPath("//tr[4]/td/b")).Text);
                Assert.AreEqual("Publisher:", driver.FindElement(By.XPath("//tr[6]/td/b")).Text);
                Assert.AreEqual("Churchill Capital", driver.FindElement(By.XPath("//tr[6]/td[3]")).Text);
                Assert.AreEqual("The following prerequisites are required:", driver.FindElement(By.XPath("//table[@id='BootstrapperSection']/tbody/tr/td")).Text);
                Assert.AreEqual("If these components are already installed, you can launch the application now. Otherwise, click the button below to install the prerequisites and run the application.", driver.FindElement(By.XPath("//table[@id='BootstrapperSection']/tbody/tr[3]/td")).Text);
                Assert.AreEqual(".NET Framework 2.0 (x86)", driver.FindElement(By.XPath("//table[@id='BootstrapperSection']/tbody/tr[2]/td[2]")).Text);
                Assert.AreEqual("ClickOnce and .NET Framework Resources", driver.FindElement(By.LinkText("ClickOnce and .NET Framework Resources")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            driver.Navigate().Back();
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Comps by Sector form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li[5]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Comps by Sector", driver.Title);
                Assert.AreEqual("COMPUTATION", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/table/tbody/tr/td/b")).Text);
                Assert.AreEqual("View In Excel", driver.FindElement(By.Id("ctl00_Header_lbViewInExcel")).Text);
                Assert.AreEqual("Filter by:", driver.FindElement(By.XPath("//div[@id='ctl00_F_Filters']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Enter Tickers:", driver.FindElement(By.XPath("//div[@id='ctl00_F_Filters']/table/tbody/tr/td[2]")).Text);
                Assert.AreEqual("(Please use ',' to separate tickers)", driver.FindElement(By.XPath("//div[@id='ctl00_F_Filters']/table/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Add Company", driver.FindElement(By.Id("ctl00_F_lbAdd")).Text);
                Assert.AreEqual("Company Name", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Last Updated", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Ticker", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Sub-Industry", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[6]")).Text);
                Assert.AreEqual("Price", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Shares Out", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[8]")).Text);
                Assert.AreEqual("Market Cap", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Debt", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[10]")).Text);
                Assert.AreEqual("Minority Interests", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[11]")).Text);
                Assert.AreEqual("Cash", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[12]")).Text);
                Assert.AreEqual("Enterprise Val", driver.FindElement(By.XPath("//div[@id='ctl00_PH_P1']/table/tbody/tr/td[13]")).Text);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            */
            currentFormOfChecking = "Merger Analytics form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/div/ul/li[6]/a/span");
           
            try
            {
                checkOnError(currentFormOfChecking);
            }
            catch (Exception e)
            {
                Assert.Fail("Exception during the login: " + e.ToString());
            }

            while (!IsElementVisible(driver.FindElement(By.XPath("//tr[@id='ctl00_propertyHeaderRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td[6]")))) ;
            try
            {                
                Assert.AreEqual("Merger Analytics", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/table/tbody/tr/td/b")).Text);
                Assert.AreEqual("View In Excel", driver.FindElement(By.Id("ctl00_Header_lbViewInExcel")).Text);
                Assert.AreEqual("Merger Analytics", driver.Title);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_Reset")).Text);
                Assert.AreEqual("Industry", driver.FindElement(By.LinkText("Industry")).Text);
                Assert.AreEqual("Sub-Industry", driver.FindElement(By.LinkText("Sub-Industry")).Text);
                Assert.AreEqual("Announcement Date", driver.FindElement(By.LinkText("Announcement Date")).Text);
                Assert.AreEqual("Est Closing Date", driver.FindElement(By.LinkText("Est Closing Date")).Text);
                Assert.AreEqual("Target Name", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("Acquirer Name", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[6]")).Text);
                Assert.AreEqual("Acquirer Type", driver.FindElement(By.LinkText("Acquirer Type")).Text);
                Assert.AreEqual("Deal Status", driver.FindElement(By.LinkText("Deal Status")).Text);
                Assert.AreEqual("FX", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[9]")).Text);
                Assert.AreEqual("Deal EV", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[10]")).Text);
                Assert.AreEqual("Foward EV/ EBITDA", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[11]")).Text);
                Assert.AreEqual("Forward P/E", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[12]")).Text);
                Assert.AreEqual("Forward EV/S", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[13]")).Text);
                Assert.AreEqual("Forward P/B", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[14]")).Text);
                Assert.AreEqual("% premium to undistributed price", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[15]")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_PH_gvMergerAnalitics']/tbody/tr/th[16]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_04_DistributionTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            currentFormOfChecking = "Trading Notes form";
            addLogInformation("DISTRIBUTION TAB");
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li/a/span");

            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add/Edit trading note item", driver.FindElement(By.Id("ctl00_F_lAddTradingNote")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gD']/tbody/tr/th")).Text);
                Assert.AreEqual("Trading note draft", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gD']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gD']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gD']/tbody/tr/th")).Text);
                //Assert.AreEqual("Europe", driver.FindElement(By.Id("ctl00_MC_gD_ctl02_rhlDraftRegion")).Text);
                //Assert.AreEqual("United States", driver.FindElement(By.Id("ctl00_MC_gD_ctl03_rhlDraftRegion")).Text);
                //Assert.AreEqual("Asia-Pacific", driver.FindElement(By.Id("ctl00_MC_gD_ctl04_rhlDraftRegion")).Text);
                Assert.AreEqual("Edit Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Pdf Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Email Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Reactivate Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Trading Notes", driver.Title);
                Assert.AreEqual("Archive", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th")).Text);
                Assert.AreEqual("Trading note", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Unarchive", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th[4]")).Text);
                Assert.AreEqual("Date:", driver.FindElement(By.Id("ctl00_B_lFilter")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_B_btnFilterReset")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Morning Notes form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Morning Notes", driver.Title);
                Assert.AreEqual("Add/Edit morning note item", driver.FindElement(By.Id("ctl00_F_lAddMorningNote")).Text);
                Assert.AreEqual("Edit Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Pdf Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Email Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Reactivate Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[5]")).Text);
                Assert.AreEqual("View as Plain Text", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[6]")).Text);
                Assert.AreEqual("Add to Archive", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Archive", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th")).Text);
                Assert.AreEqual("Morning note", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_P_dA']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Date:", driver.FindElement(By.Id("ctl00_B_lFilter")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_B_btnFilterReset")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Research Box form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Research Box", driver.Title);
                Assert.AreEqual("RESEARCH BOX", driver.FindElement(By.Id("ctl00_Header_lbAction")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.Id("ctl00_F_lbticker")).Text);
                Assert.AreEqual("Title:", driver.FindElement(By.Id("ctl00_F_lbTitle")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.Id("ctl00_F_lbregion")).Text);
                Assert.AreEqual("From", driver.FindElement(By.Id("ctl00_F_lbFrom")).Text);
                Assert.AreEqual("To", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnResearchBox']/table/tbody/tr/td[11]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_btReset")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.LinkText("Idea")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.LinkText("Title")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Sent on", driver.FindElement(By.LinkText("Sent on")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_strategyResearchList_gvList']/tbody/tr/th[6]")).Text);
                Assert.AreEqual("Edit Research Box", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete Research Box", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Send Email", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Research Editor", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Synopsis:", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lSynopsis")).Text);
                Assert.AreEqual("Plain Text", driver.FindElement(By.XPath("//table[@id='ctl00_P_strategyResearchEdit_rblPlainText']/tbody/tr/td/label")).Text);
                Assert.AreEqual("Html", driver.FindElement(By.XPath("//table[@id='ctl00_P_strategyResearchEdit_rblPlainText']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("Exclude from interactions (invitation to dinner or call, etc.)", driver.FindElement(By.XPath("//div[@id='ctl00_P_pE']/table/tbody/tr/td[2]/span/label")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lTitle")).Text, "^Title:[\\s\\S]*$"));
                Assert.AreEqual("Idea:", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lIdea")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lRegion")).Text, "^Region:[\\s\\S]*$"));
                Assert.AreEqual("Disclaimer:", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_Label1")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lReportType")).Text, "^Report:[\\s\\S]*$"));
                Assert.AreEqual("Strategy:", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lSubstrategy")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_lType")).Text, "^Type:[\\s\\S]*$"));
                Assert.AreEqual("Publication Delay:", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_Label2")).Text);
                Assert.AreEqual("Upload a new file", driver.FindElement(By.Id("ctl00_P_strategyResearchEdit_uploadTool_hlUpload2")).Text);
                Assert.AreEqual("Files", driver.FindElement(By.LinkText("Files")).Text);
                Assert.AreEqual("No files uploaded", driver.FindElement(By.XPath("//table[@id='ctl00_P_strategyResearchEdit_fileList_gwFiles']/tbody/tr/td/span")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Quick Notes form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Quick Notes", driver.Title);
                Assert.AreEqual("Quick Note", driver.FindElement(By.Id("ctl00_Header_lbAction")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.LinkText("Title")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Sent on", driver.FindElement(By.LinkText("Sent on")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_macroWatchList_gwMacroWatchList']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("Edit Quick Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete Quick Note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Send Email", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Quick Note Editor", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_macroWatchEdit_lTitle")).Text, "^Title:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pE']/table/tbody/tr[2]/td")).Text, "^Region:[\\s\\S]*$"));
                Assert.AreEqual("Description:", driver.FindElement(By.Id("ctl00_P_macroWatchEdit_lDescription")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_macroWatchEdit_lContent")).Text, "^Content:[\\s\\S]*$"));
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Strategic Research view report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[5]/a/span");

            ClickCurrentElement(By.Id("ctl00_F_DDListRes"));
            new SelectElement(driver.FindElement(By.Id("ctl00_F_DDListRes"))).SelectByText("..No filter ..");
            ClickCurrentElement(By.Id("ctl00_F_DDListRes"));
            ClickCurrentElement(By.Id("ctl00_F_btFind"));
            WaitForElement(By.LinkText("Company Name"));

            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Strategy Research view report", driver.Title);
                Assert.AreEqual("Strategy Research view report", driver.FindElement(By.Id("ctl00_F_lbAction")).Text);
                Assert.AreEqual("Research Subject:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Status:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[5]")).Text);
                Assert.AreEqual("Sent On:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[7]")).Text);
                Assert.AreEqual("View by Client", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPL']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Company Name", driver.FindElement(By.LinkText("Company Name")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("Research Subject", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gr']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Last sent", driver.FindElement(By.LinkText("Last sent")).Text);
                Assert.AreEqual("Read on", driver.FindElement(By.LinkText("Read on")).Text);
                Assert.AreEqual("# of times", driver.FindElement(By.LinkText("# of times")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Morning Note view report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[6]/a/span");
            try
            {
                Thread.Sleep(2000);
                checkOnError(currentFormOfChecking);
                new SelectElement(driver.FindElement(By.Id("ctl00_F_DDListRes"))).SelectByText("..No filter ..");
                driver.FindElement(By.Id("ctl00_F_DDListRes")).Click();
                driver.FindElement(By.Id("ctl00_F_btFind")).Click();
                Thread.Sleep(2000);
                Assert.AreEqual("Morning Note view report", driver.Title);
                Assert.AreEqual("Morning Note view report", driver.FindElement(By.Id("ctl00_F_lbAction")).Text);
                Assert.AreEqual("Morning Note:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Status:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[5]")).Text);
                Assert.AreEqual("Sent On:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[7]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_btReset")).Text);
                Assert.AreEqual("View by Client", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPL']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Company Name", driver.FindElement(By.LinkText("Company Name")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("Morning note", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gr']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Sent on", driver.FindElement(By.LinkText("Sent on")).Text);
                Assert.AreEqual("Read on", driver.FindElement(By.LinkText("Read on")).Text);
                Assert.AreEqual("# of times", driver.FindElement(By.LinkText("# of times")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Trading Note view report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[4]/div/ul/li[7]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Trading Note view report", driver.Title);
                Assert.AreEqual("Trading Note view report", driver.FindElement(By.Id("ctl00_F_lbAction")).Text);
                Assert.AreEqual("Trading Note:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Status:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[5]")).Text);
                Assert.AreEqual("Sent On:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr[2]/td[7]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_btReset")).Text);
                Assert.AreEqual("Trading Note view report", driver.Title);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_05_MonitorsTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("MONITORS TAB");
            currentFormOfChecking = "Monitor Manager form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Monitor Manager", driver.Title);
                Assert.AreEqual("MONITOR MANAGER", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Templates", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Template Editor", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Create New Template", driver.FindElement(By.Id("ctl00_MC_templateList_lbNewTemplate")).Text);
                Assert.AreEqual("Template:", driver.FindElement(By.Id("ctl00_MC_templateEditor_lbTemplateName")).Text);
                Assert.AreEqual("Save As", driver.FindElement(By.Id("ctl00_MC_templateEditor_lbSaveAs")).Text);
                Assert.AreEqual("Save", driver.FindElement(By.Id("ctl00_MC_templateEditor_lbSave")).Text);
                Assert.AreEqual("Monitor Category", driver.FindElement(By.XPath("//table[@id='maintable']/thead/tr/td")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='maintable']/thead/tr/td[2]")).Text);
                Assert.AreEqual("View Ideas", driver.FindElement(By.XPath("//table[@id='maintable']/thead/tr/td[3]")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='maintable']/thead/tr/td[4]")).Text);
                Assert.AreEqual("Step 3:", driver.FindElement(By.XPath("//tr[@id='steps']/td[4]/b")).Text);
                Assert.AreEqual("Step 2:", driver.FindElement(By.XPath("//tr[@id='steps']/td[2]/b")).Text);
                Assert.AreEqual("No category selected", driver.FindElement(By.XPath("//tr[@id='empty']/td")).Text);
                Assert.AreEqual("Step 1:", driver.FindElement(By.XPath("//tr[@id='steps']/td/b")).Text);
                Assert.AreEqual("Document Date:", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td[2]/div[5]/div[3]/label")).Text);
                Assert.AreEqual("Document Title (optional):", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/table/tbody/tr/td[2]/div[5]/div[2]/label")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.XPath("//table[@id='ctl00_MC_templateList_mgvTemplates']/tbody/tr/th")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_templateList_mgvTemplates']/tbody/tr/th[2]")).Text);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Company Events form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Company Events", driver.Title);
                Assert.AreEqual("COMPANY EVENTS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnCompanyEventFilter']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Event:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnCompanyEventFilter']/table/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Date From:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnCompanyEventFilter']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Date To:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnCompanyEventFilter']/table/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Europe", driver.FindElement(By.Id("ctl00_MC_rpRegion_ctl01_lbtRegion")).Text);
                Assert.AreEqual("United States", driver.FindElement(By.Id("ctl00_MC_rpRegion_ctl02_lbtRegion")).Text);
                Assert.AreEqual("Asia-Pacific", driver.FindElement(By.Id("ctl00_MC_rpRegion_ctl03_lbtRegion")).Text);
                Assert.AreEqual("Cross Border", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPCompany']/table/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gvList']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("Event", driver.FindElement(By.LinkText("Event")).Text);
                Assert.AreEqual("Ticker", driver.FindElement(By.LinkText("Ticker")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Days Left", driver.FindElement(By.LinkText("Days Left")).Text);
                Assert.AreEqual("Company Event", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPadd']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPadd']/table[2]/tbody/tr/td")).Text, "^Date:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPadd']/table[2]/tbody/tr[3]/td")).Text, "^Region:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPadd']/table[2]/tbody/tr[4]/td")).Text, "^Ticker:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPadd']/table[2]/tbody/tr/td[4]")).Text, "Event"));
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();            

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_06_ClientBoxTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            currentFormOfChecking = @"Company/Client List form";
            addLogInformation("CLIENT BOX TAB");
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Company/Client List", driver.Title);
                Assert.AreEqual("By city:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("By company:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[6]")).Text);
                Assert.AreEqual("By client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("OR/AND", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[2]/b")).Text);
                Assert.AreEqual("OR/AND", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[4]/b")).Text);
                Assert.AreEqual("OR", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[7]/b")).Text);
                Assert.AreEqual("Add New Client", driver.FindElement(By.Id("ctl00_F_AddClient")).Text);
                Assert.AreEqual("Add New Company", driver.FindElement(By.Id("ctl00_F_AddCompany")).Text);
                Assert.AreEqual("A", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl00_lAlphabet")).Text);
                Assert.AreEqual("Companies/Clients", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPClients']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Phone Numbers", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPClients']/table/tbody/tr/td[2]/b")).Text);
                Assert.AreEqual("B", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl01_lAlphabet")).Text);
                Assert.AreEqual("C", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl02_lAlphabet")).Text);
                Assert.AreEqual("D", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl03_lAlphabet")).Text);
                Assert.AreEqual("F", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl05_lAlphabet")).Text);
                Assert.AreEqual("G", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl06_lAlphabet")).Text);
                Assert.AreEqual("H", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl07_lAlphabet")).Text);
                Assert.AreEqual("K", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl10_lAlphabet")).Text);
                Assert.AreEqual("L", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl11_lAlphabet")).Text);
                Assert.AreEqual("M", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl12_lAlphabet")).Text);
                Assert.AreEqual("N", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl13_lAlphabet")).Text);
                Assert.AreEqual("O", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl14_lAlphabet")).Text);
                Assert.AreEqual("Q", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl16_lAlphabet")).Text);
                Assert.AreEqual("R", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl17_lAlphabet")).Text);
                Assert.AreEqual("T", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl19_lAlphabet")).Text);
                Assert.AreEqual("U", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl20_lAlphabet")).Text);
                Assert.AreEqual("W", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl22_lAlphabet")).Text);
                Assert.AreEqual("X", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl23_lAlphabet")).Text);
                Assert.AreEqual("Y", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl24_lAlphabet")).Text);
                Assert.AreEqual("Z", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl25_lAlphabet")).Text);
                Assert.AreEqual("J", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl09_lAlphabet")).Text);
                Assert.AreEqual("S", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl18_lAlphabet")).Text);
                Assert.AreEqual("E", driver.FindElement(By.Id("ctl00_MC_rAlphabet_ctl04_lAlphabet")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Company/Client Filter form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Company/Client Filter", driver.Title);
                Assert.AreEqual("Companies/Clients", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPClients']/table/tbody/tr/td/b")).Text);
                Assert.AreEqual("Phone Numbers", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPClients']/table/tbody/tr/td[2]/b")).Text);
                Assert.AreEqual("OR/AND", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[2]/b")).Text);
                Assert.AreEqual("By city:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("OR/AND", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[4]/b")).Text);
                Assert.AreEqual("By company:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[6]")).Text);
                Assert.AreEqual("OR", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[7]/b")).Text);
                Assert.AreEqual("By client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pFLT']/div/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Add New Client", driver.FindElement(By.Id("ctl00_F_AddClient")).Text);
                Assert.AreEqual("Back To Top", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPClients']/a/b")).Text);
                Assert.AreEqual("Add New Company", driver.FindElement(By.Id("ctl00_F_AddCompany")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Add New Company form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add New Company", driver.Title);
                Assert.AreEqual("Company Details:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_CompanyDet_T']/em")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr/td/label")).Text, "^Company Name:[\\s\\S]*$"));
                Assert.AreEqual("Address:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("City:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[6]/td/label")).Text);
                Assert.AreEqual("State:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[7]/td/label")).Text);
                Assert.AreEqual("Postcode/ZIP:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[8]/td/label")).Text);
                Assert.AreEqual("Country:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[9]/td/label")).Text);
                Assert.AreEqual("Website:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[10]/td/label")).Text);
                Assert.AreEqual("Founders:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr/td[3]/label")).Text);
                Assert.AreEqual("Operations:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[2]/td[3]/label")).Text);
                Assert.AreEqual("Opened with Churchill Capital Ltd.:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[3]/td[2]/label")).Text);
                Assert.AreEqual("Fund Classification:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[4]/td[2]/label")).Text);
                Assert.AreEqual("Bloomberg cust.#:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[6]/td[3]/label")).Text);
                Assert.AreEqual("Bigdough Client:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[7]/td[3]/label")).Text);
                Assert.AreEqual("Short Name:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[8]/td[3]/label")).Text);
                Assert.AreEqual("Europe", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr/td[5]/span/label")).Text);
                Assert.AreEqual("Asia-Pacific", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr/td[6]/span/label")).Text);
                Assert.AreEqual("US", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr/td[7]/span/label")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[4]/td[4]/label")).Text, "^Revenue Category:[\\s\\S]*$"));
                Assert.AreEqual("Comments:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[5]/td[4]/label")).Text);
                Assert.AreEqual("Interactions Recipient:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[10]/td[4]/label")).Text);
                Assert.AreEqual("Interactions Shortname:", driver.FindElement(By.XPath("//div[@id='CompanyDetailsTemplate']/div/table/tbody/tr[11]/td[5]/label")).Text);
                Assert.AreEqual("COMPANY/CLIENT DETAILS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/span")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Add New Client form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add New Client", driver.FindElement(By.XPath("//form[@id='form1']/div[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Select Company Name:", driver.FindElement(By.Id("AddNewClientControl_lbSelectCompany")).Text);
                Assert.AreEqual("Add New Client", driver.Title);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Move Client to Company form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[5]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Move Client to Company", driver.Title);
                Assert.AreEqual("Move Client to Company", driver.FindElement(By.XPath("//form[@id='form1']/div[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Select Client Name:", driver.FindElement(By.Id("AddClientToCompany_lbSelectClient")).Text);
                Assert.AreEqual("Select Company Name:", driver.FindElement(By.Id("AddClientToCompany_lbSelectCompany")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Merge Clients form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[6]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Merge Clients", driver.Title);
                Assert.AreEqual("Merge Clients", driver.FindElement(By.XPath("//form[@id='aspnetForm']/div[2]/div/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Source Client:", driver.FindElement(By.Id("ctl00_body_lblSourceClient")).Text);
                Assert.AreEqual("Target Client:", driver.FindElement(By.Id("ctl00_body_lblTragetClient")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            closeTheForm(mainWeb);
            Console.WriteLine();

            currentFormOfChecking = "Cold Call Monitor form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[7]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Cold Call Monitor", driver.Title);
                Assert.AreEqual("Cold Call Monitor List", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPC']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Company Name", driver.FindElement(By.LinkText("Company Name")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("OtherInfo", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CCMonitorList_ColdCallMonitor']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Interested", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CCMonitorList_ColdCallMonitor']/tbody/tr/th[4]")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("User Name", driver.FindElement(By.LinkText("User Name")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td")).Text, "^Company Name:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[2]/td")).Text, "^Client Name:[\\s\\S]*$"));
                Assert.AreEqual("Phone #:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[3]/td")).Text);
                Assert.AreEqual("User:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Other Info:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[5]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Discussions form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
               "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[8]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                WaitForElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div"));
                Assert.AreEqual("Client Discussions", driver.Title);
                Assert.AreEqual("CLIENT DISCUSSIONS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Event:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Date Range:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("User Name:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.LinkText("Idea")).Text);
                Assert.AreEqual("Discussion", driver.FindElement(By.LinkText("Discussion")).Text);
                Assert.AreEqual("User Name", driver.FindElement(By.LinkText("User Name")).Text);
                Assert.AreEqual("Discussion Type", driver.FindElement(By.LinkText("Discussion Type")).Text);
                Assert.AreEqual("Duration (min)", driver.FindElement(By.LinkText("Duration (min)")).Text);
                Assert.AreEqual("1", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td/span")).Text);
                Assert.AreEqual("2", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[2]/a")).Text);
                Assert.AreEqual("3", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[3]/a")).Text);
                Assert.AreEqual("4", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[4]/a")).Text);
                Assert.AreEqual("5", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[5]/a")).Text);
                Assert.AreEqual("6", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[6]/a")).Text);
                Assert.AreEqual("7", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[7]/a")).Text);
                Assert.AreEqual("8", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[8]/a")).Text);
                Assert.AreEqual("9", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[9]/a")).Text);
                Assert.AreEqual("10", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[30]/td/table/tbody/tr/td[10]/a")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Meetings form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
               "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[9]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Meetings", driver.Title);
                Assert.AreEqual("CLIENT MEETINGS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Date From:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Date To:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Attendee:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Date and Time", driver.FindElement(By.LinkText("Date and Time")).Text);
                Assert.AreEqual("Ideas", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Clients", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Attendees", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr/th[4]")).Text);
                Assert.AreEqual("Address", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("Meeting Notes", driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr/th[6]")).Text);
                Assert.AreEqual("User Name", driver.FindElement(By.LinkText("User Name")).Text);
                Assert.AreEqual("Duration", driver.FindElement(By.LinkText("Duration")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Groups form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
               "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[10]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Groups", driver.Title);
                Assert.AreEqual("CLIENT GROUPS", driver.FindElement(By.Id("ctl00_Header_lbAction")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.LinkText("Title")).Text);
                Assert.AreEqual("Clients", driver.FindElement(By.XPath("//table[@id='ctl00_MC_clientGroupList_gwClientGroupList']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Created by", driver.FindElement(By.LinkText("Created by")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_clientGroupList_gwClientGroupList']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("Client Groups", driver.Title);
                Assert.AreEqual("Edit Group", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete Group", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Client Group Editor", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_clientGroupEdit_LabelClientsGroupName")).Text, "^Title:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.Id("ctl00_P_clientGroupEdit_LabelListOfClients")).Text, "^List of Clients:[\\s\\S]*$"));
                Assert.AreEqual("Client", driver.FindElement(By.XPath("//table[@id='ctl00_P_clientGroupEdit_ListOfClients_gvList']/tbody/tr/th")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_P_clientGroupEdit_ListOfClients_gvList']/tbody/tr/th[2]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_07_ToolsTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("TOOLS TAB");
            currentFormOfChecking = "Web Links form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Web Links", driver.Title);
                Assert.AreEqual("Filter List    ", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td/b")).Text);
                Assert.AreEqual("Description:  ", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_LbRe")).Text);
                Assert.AreEqual("Web Link Details", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr/td")).Text, "^Category:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[2]/td")).Text, "^Page Title:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[3]/td")).Text, "^Page Url:[\\s\\S]*$"));
                Assert.AreEqual("Expire Date:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Cost:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Website Login:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[3]/td[3]")).Text);
                Assert.AreEqual("Website Password:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr[4]/td[3]")).Text);
                Assert.AreEqual("Description:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPP']/table/tbody/tr/td/table[2]/tbody/tr/td[5]")).Text);
            }
            catch (Exception e)
            {

                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Keyword Search form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Keyword Search", driver.Title);
                Assert.AreEqual("Keyword Search", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Ticker, company name or keyword:", driver.FindElement(By.XPath("//div[@id='ctl00_PH_pnGlobalsearch']/table/tbody/tr/td[2]/div/div")).Text);
                Assert.AreEqual("Exact match", driver.FindElement(By.XPath("//div[@id='ctl00_PH_pnGlobalsearch']/table/tbody/tr[2]/td[2]/span/label")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_PH_Reset")).Text);
                Assert.AreEqual("Scope", driver.FindElement(By.XPath("//tr[@id='ctl00_propertyHeaderRow']/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Search results", driver.FindElement(By.XPath("//tr[@id='ctl00_propertyHeaderRow']/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Idea / Ticker", driver.FindElement(By.LinkText("Idea / Ticker")).Text);
                Assert.AreEqual("Description", driver.FindElement(By.XPath("//table[@id='ctl00_PH_lvItemsTable_ctl00']/thead/tr/th[3]")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Location", driver.FindElement(By.LinkText("Location")).Text);
                Assert.AreEqual("No matches found.", driver.FindElement(By.XPath("//table[@id='ctl00_PH_lvItemsTable_ctl00']/tbody/tr/td")).Text);
                Assert.AreEqual("0 items found", driver.FindElement(By.XPath("//table[@id='ctl00_PH_lvItemsTable_ctl00']/tfoot/tr/td[3]")).Text);
                Assert.AreEqual("Unselect All", driver.FindElement(By.LinkText("Unselect All")).Text);
                Assert.AreEqual("Select All", driver.FindElement(By.LinkText("Select All")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//tr[@id='ctl00_propertyHeaderRow']/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div[2]")).Text);
                Assert.AreEqual("Europe", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Regions']/tbody/tr/td/span/label")).Text);
                Assert.AreEqual("United States", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Regions']/tbody/tr[2]/td/span/label")).Text);
                Assert.AreEqual("Asia-Pacific", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Regions']/tbody/tr[3]/td/span/label")).Text);
                Assert.AreEqual("Cross Border", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Regions']/tbody/tr[4]/td/span/label")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr/td/span/label")).Text);
                Assert.AreEqual("Research Box", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[2]/td/span/label")).Text);
                Assert.AreEqual("Commentary Developments", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[3]/td/span/label")).Text);
                Assert.AreEqual("Morning Note", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[4]/td/span/label")).Text);
                Assert.AreEqual("Trading Note", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[5]/td/span/label")).Text);
                Assert.AreEqual("Client Box", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[6]/td/span/label")).Text);
                Assert.AreEqual("Client Discussions", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[7]/td/span/label")).Text);
                Assert.AreEqual("Client Meetings", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[8]/td/span/label")).Text);
                Assert.AreEqual("Events Calendar", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[9]/td/span/label")).Text);
                Assert.AreEqual("Address Book", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[10]/td/span/label")).Text);
                Assert.AreEqual("Announcements", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[11]/td/span/label")).Text);
                Assert.AreEqual("Research To Do", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[12]/td")).Text);
                Assert.AreEqual("Cold Call Monitor", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[13]/td/span/label")).Text);
                Assert.AreEqual("Web Links", driver.FindElement(By.XPath("//table[@id='ctl00_PH_Sections']/tbody/tr[14]/td/span/label")).Text);
                Assert.AreEqual("Location", driver.FindElement(By.XPath("//tr[@id='ctl00_propertyHeaderRow']/td/table/tbody/tr[2]/td/table/tbody/tr[2]/td/div[3]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Address Book form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("First Name or Last Name:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pF']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Company:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pF']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Idea:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pF']/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("Classification:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pF']/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("First Name", driver.FindElement(By.LinkText("First Name")).Text);
                Assert.AreEqual("Last Name", driver.FindElement(By.LinkText("Last Name")).Text);
                Assert.AreEqual("Company", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gAB']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Position", driver.FindElement(By.LinkText("Position")).Text);
                Assert.AreEqual("Work", driver.FindElement(By.LinkText("Work")).Text);
                Assert.AreEqual("Mobile", driver.FindElement(By.LinkText("Mobile")).Text);
                Assert.AreEqual("Email", driver.FindElement(By.LinkText("Email")).Text);
                Assert.AreEqual("Related Ideas", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gAB']/tbody/tr/th[8]")).Text);
                Assert.AreEqual("10", driver.FindElement(By.LinkText("10")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
                Assert.AreEqual("Delete", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Add Address Book Entry", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr/td")).Text, "^First Name:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[2]/td")).Text, "^Last Name:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[3]/td")).Text, "^Company:[\\s\\S]*$"));
                Assert.AreEqual("Category:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Address:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[5]/td")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[6]/td")).Text, "^Classification:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr/td[3]")).Text, "^Position:[\\s\\S]*$"));
                Assert.AreEqual("Email:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[2]/td[3]")).Text);
                Assert.AreEqual("Mobile:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[3]/td[3]")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[4]/td[3]")).Text, "^Work:[\\s\\S]*$"));
                Assert.AreEqual("Fax:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr[5]/td[3]")).Text);
                Assert.AreEqual("Comments:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pG']/table[2]/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Ideas", driver.FindElement(By.XPath("//table[@id='ctl00_P_listIdeas_gvList']/tbody/tr/th")).Text);
                Assert.AreEqual("Action", driver.FindElement(By.XPath("//table[@id='ctl00_P_listIdeas_gvList']/tbody/tr/th[2]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Meeting Scheduler form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Meeting Scheduler", driver.Title);
                Assert.AreEqual("Meeting Scheduler List", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPM']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Meeting Date", driver.FindElement(By.LinkText("Meeting Date")).Text);
                Assert.AreEqual("Meeting Time", driver.FindElement(By.LinkText("Meeting Time")).Text);
                Assert.AreEqual("Company", driver.FindElement(By.LinkText("Company")).Text);
                Assert.AreEqual("Primary Attendee", driver.FindElement(By.LinkText("Primary Attendee")).Text);
                Assert.AreEqual("Address", driver.FindElement(By.LinkText("Address")).Text);
                Assert.AreEqual("Comments", driver.FindElement(By.LinkText("Comments")).Text);
                Assert.AreEqual("Who Entered", driver.FindElement(By.LinkText("Who Entered")).Text);
                Assert.AreEqual("Meeting Scheduler Details", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[2]")).Text, "Address:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[2]/span[2]/label")).Text, "Meeting Confirmed by Primary Contact"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[3]")).Text, "Company Name:"));
                Assert.AreEqual("Primary Contact:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[4]/td[2]")).Text);
                Assert.AreEqual("Other:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[5]/td[2]")).Text);
                Assert.AreEqual("Meeting Date: ", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Meeting Time: ", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Comments: ", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[3]/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Announcements form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[5]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Date From:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnAnnouncements']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Date To:", driver.FindElement(By.XPath("//div[@id='ctl00_F_pnAnnouncements']/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Announcement List", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPList']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Start Date", driver.FindElement(By.LinkText("Start Date")).Text);
                Assert.AreEqual("End date", driver.FindElement(By.LinkText("End date")).Text);
                Assert.AreEqual("Who", driver.FindElement(By.LinkText("Who")).Text);
                Assert.AreEqual("Event", driver.FindElement(By.LinkText("Event")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
                Assert.AreEqual("Announcement Details", driver.FindElement(By.XPath("//div[@id='ctl00_P_UPEdit']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPEdit']/table[2]/tbody/tr/td")).Text, "^Start Date:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPEdit']/table[2]/tbody/tr[2]/td")).Text, "^End Date:[\\s\\S]*$"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPEdit']/table[2]/tbody/tr[3]/td")).Text, "Staff:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UPEdit']/table[2]/tbody/tr/td[3]")).Text, "Event:"));
                Assert.AreEqual("Announcements", driver.Title);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Research To Do form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[6]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Research To Do", driver.Title);
                Assert.AreEqual("Research To Do List", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UPC']/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Research To Do", driver.Title);
                Assert.AreEqual("Research To Do Details", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td")).Text, "Ticker:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[2]/td")).Text, "Title:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[3]/td")).Text, "Type:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[4]/td")).Text, "Status:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[4]")).Text, "Due Date:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr[2]/td[4]")).Text, "Region"));
                Assert.AreEqual("Comments:", driver.FindElement(By.XPath("//div[@id='ctl00_P_UP']/table[2]/tbody/tr/td[7]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Document Manager form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[7]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Document Manager", driver.Title);
                Assert.AreEqual("Document Manager", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Folders", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel1']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("You're looking at folder: root", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel2']/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Search for file:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr/td")).Text);
                Assert.AreEqual("in current folder only", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/span/label")).Text);
                Assert.AreEqual("Folders management", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel1']/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Create subfolder:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel1']/table/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Rename folder to:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel1']/table/tbody/tr[7]/td")).Text);
                Assert.AreEqual("Delete folder:", driver.FindElement(By.XPath("//div[@id='ctl00_MC_UpdatePanel1']/table/tbody/tr[10]/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Tickers form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Tickers", driver.Title);
                Assert.AreEqual("TICKERS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Company:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span/span")).Text);
                Assert.AreEqual("IsValid:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[2]/label")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[3]/label")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[4]/span")).Text);
                Assert.AreEqual("Ticker", driver.FindElement(By.LinkText("Ticker")).Text);
                Assert.AreEqual("Id", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th")).Text);
                Assert.AreEqual("DateCreated", driver.FindElement(By.LinkText("DateCreated")).Text);
                Assert.AreEqual("Company", driver.FindElement(By.LinkText("Company")).Text);
                Assert.AreEqual("IsValid", driver.FindElement(By.LinkText("IsValid")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[7]")).Text);
                Assert.AreEqual("Delete Tickers", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Tickers", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("10", driver.FindElement(By.LinkText("10")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the ticker");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add new entry to table Tickers", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Id", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
                Assert.AreEqual("Ticker", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("DateCreated", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[3]/td/label")).Text);
                Assert.AreEqual("Company", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[4]/td/label")).Text);
                Assert.AreEqual("IsValid", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[5]/td/label")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[6]/td/label")).Text);
                Assert.AreEqual("Insert Cancel", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[7]/td")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Featured News form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[2]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Featured News", driver.Title);
                Assert.AreEqual("FEATURED NEWS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Enabled:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span/label")).Text);
                Assert.AreEqual("Type:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[2]/label")).Text);
                Assert.AreEqual("Title:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[3]/span")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Title", driver.FindElement(By.LinkText("Title")).Text);
                Assert.AreEqual("Type", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Enabled", driver.FindElement(By.LinkText("Enabled")).Text);
                Assert.AreEqual("First Day (GMT)", driver.FindElement(By.LinkText("First Day (GMT)")).Text);
                Assert.AreEqual("Last Day (GMT)", driver.FindElement(By.LinkText("Last Day (GMT)")).Text);
                Assert.AreEqual("News Date", driver.FindElement(By.LinkText("News Date")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[7]")).Text);
                Assert.AreEqual("Delete Featured News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Featured News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Featured News");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Edit FeaturedNewsItem", driver.Title);
                Assert.AreEqual("  Featured News Editor", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]")).Text);
                Assert.AreEqual("Type:", driver.FindElement(By.Id("ctl00_MC_lblStructuredNewsType")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Structured News Type form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[3]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("STRUCTUREDNEWSTYPE", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.LinkText("Name")).Text);
                Assert.AreEqual("StructuredNews", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Delete StructuredNewsType", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit StructuredNewsType", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[3]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Structured News");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add new entry to table StructuredNewsType", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Source of News Type form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[4]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Source of News", driver.Title);
                Assert.AreEqual("SOURCE OF NEWS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Name:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span/span")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.LinkText("Name")).Text);
                Assert.AreEqual("Url", driver.FindElement(By.LinkText("Url")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("10", driver.FindElement(By.LinkText("10")).Text);
                Assert.AreEqual("...", driver.FindElement(By.LinkText("...")).Text);
                Assert.AreEqual("Delete Source of News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Source of News", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Source of News");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Source of News", driver.Title);
                Assert.AreEqual("Url", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
                Assert.AreEqual("Add new entry to table Source of News", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Strategy Idea Type form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[5]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("STRATEGY IDEA TYPE", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("IsPrimary:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span/label")).Text);
                Assert.AreEqual("Name:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[2]/span")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.LinkText("Name")).Text);
                Assert.AreEqual("IsPrimary", driver.FindElement(By.LinkText("IsPrimary")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("2", driver.FindElement(By.LinkText("2")).Text);
                Assert.AreEqual("Delete Strategy Idea Type", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Strategy Idea Type", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Strategy Idea Type");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add new entry to table Strategy Idea Type", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
                Assert.AreEqual("IsPrimary", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Strategy Idea Type Mappings form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[6]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("STRATEGY IDEA TYPE MAPPINGS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Strategy:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span/label")).Text);
                Assert.AreEqual("Ticker type:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[2]/label")).Text);
                Assert.AreEqual("Substrategy:", driver.FindElement(By.XPath("//div[@id='ListFilters']/span[3]/label")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Ticker type", driver.FindElement(By.LinkText("Ticker type")).Text);
                Assert.AreEqual("Strategy", driver.FindElement(By.LinkText("Strategy")).Text);
                Assert.AreEqual("Substrategy", driver.FindElement(By.LinkText("Substrategy")).Text);
                Assert.AreEqual("SortingPriority", driver.FindElement(By.LinkText("SortingPriority")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[5]")).Text);
                Assert.AreEqual("1", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr[12]/td/table/tbody/tr/td/span")).Text);
                Assert.AreEqual("Delete Strategy idea type mappings", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Strategy idea type mappings", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Strategy Idea Type Mappings", driver.Title);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Strategy Idea Type Mappings");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add new entry to table Strategy idea type mappings", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Strategy idea type mappings", driver.Title);
                Assert.AreEqual("Ticker type", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
                Assert.AreEqual("Strategy", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("Substrategy", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[3]/td/label")).Text);
                Assert.AreEqual("SortingPriority", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[4]/td/label")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Common Dictionaries / Max RAV Configs form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/a/span",
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[9]/div/ul/li[7]/a/span", false);
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("MAXRAVCONFIGS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Id", driver.FindElement(By.LinkText("Id")).Text);
                Assert.AreEqual("Key", driver.FindElement(By.LinkText("Key")).Text);
                Assert.AreEqual("Value", driver.FindElement(By.LinkText("Value")).Text);
                Assert.AreEqual("Add New Item", driver.FindElement(By.Id("ctl00_MC_InsertHyperLink")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("//table[@id='ctl00_MC_GridView1']/tbody/tr/th[4]")).Text);
                Assert.AreEqual("Max RAV Configs", driver.Title);
                Assert.AreEqual("Edit MaxRAVConfigs", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
                Assert.AreEqual("Delete MaxRAVConfigs", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for the Max RAV Configs");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Add new entry to table MaxRAVConfigs", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/h2")).Text);
                Assert.AreEqual("Id", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr/td/label")).Text);
                Assert.AreEqual("Key", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[2]/td/label")).Text);
                Assert.AreEqual("Value", driver.FindElement(By.XPath("//table[@id='detailsTable']/tbody/tr[3]/td/label")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("//tr[@id='ctl00_propertiesRow']/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_08_ReportsTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("REPORTS TAB");
            currentFormOfChecking = "Extranet Report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Extranet Report", driver.Title);
                Assert.AreEqual("Week Month", driver.FindElement(By.Id("ctl00_MC_rblPeriod")).Text);
                Assert.AreEqual("Month", driver.FindElement(By.XPath("//table[@id='ctl00_MC_rblPeriod']/tbody/tr/td[2]/label")).Text);
                Assert.AreEqual("Most Read Notes (Europe)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[2]/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Most Read Notes (USA)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[2]/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Most Read Notes (Asia)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[2]/tbody/tr/td[3]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Login Attempts (General)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Successful Client Login Attempts (Per Day)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Successful Client Login Attempts (Per Month)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td[2]/table/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Password or user not found", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr/td")).Text);
                Assert.AreEqual("No access to extranet", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[2]/td")).Text);
                Assert.AreEqual("Account Access Expired", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[3]/td")).Text);
                Assert.AreEqual("Logged in successfully", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[4]/td")).Text);
                Assert.AreEqual("Incorrect Password", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[5]/td")).Text);
                Assert.AreEqual("Forced logout - email changed", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[3]/tbody/tr/td/table/tbody/tr/td/table[2]/tbody/tr[6]/td")).Text);
                Assert.AreEqual("Client", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gLA']/tbody/tr/th")).Text);
                Assert.AreEqual("Access", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gLA']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Date/Time", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gLA']/tbody/tr/th[3]")).Text);
                Assert.AreEqual("Client Reports", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Top 10 Active Clients", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr/td[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Top 10 Active Clients (incl. expired)", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr/td[3]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("General Client Report", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gS']/tbody/tr/th")).Text);
                Assert.AreEqual("General Client Report", driver.FindElement(By.XPath("//table[@id='ctl00_MC_MaxGridView1']/tbody/tr/th")).Text);
                Assert.AreEqual("General Client Report", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gT']/tbody/tr/th")).Text);
                Assert.AreEqual("Times Logged In", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gT']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Last 10 Clients", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr[3]/td/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Last Month Top 10 Ideas", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr[3]/td[2]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Last 10 Ideas", driver.FindElement(By.XPath("//tr[@id='ctl00_mainContentRow']/td/div/table[4]/tbody/tr[3]/td[3]/table/tbody/tr[2]/td[2]/b")).Text);
                Assert.AreEqual("Time Viewed", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gIT2']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gIT2']/tbody/tr/th")).Text);
                Assert.AreEqual("Times Viewed", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gIT']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gIT']/tbody/tr/th")).Text);
                Assert.AreEqual("Last Logged In", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gT2']/tbody/tr/th[2]")).Text);
                Assert.AreEqual("General Client Report", driver.FindElement(By.XPath("//table[@id='ctl00_MC_gT2']/tbody/tr/th")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Activity form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[2]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Activity", driver.Title);
                Assert.AreEqual("CLIENT ACTIVITY REPORT", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Date from:", driver.FindElement(By.XPath("//div[@id='filterHost']/span")).Text);
                Assert.AreEqual("Date to:", driver.FindElement(By.XPath("//div[@id='filterHost']/span[2]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='filterHost']/span[3]")).Text);
                Assert.AreEqual("Company:", driver.FindElement(By.XPath("//div[@id='filterHost']/span[4]")).Text);
                Assert.AreEqual("Title Filter:", driver.FindElement(By.XPath("//div[@id='filterHost']/span[5]")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.XPath("//div[@id='filterHost']/span[6]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.LinkText("Reset")).Text);
                Assert.AreEqual("Company/Client", driver.FindElement(By.LinkText("Company/Client")).Text);
                Assert.AreEqual("Research Sent", driver.FindElement(By.LinkText("Research Sent")).Text);
                Assert.AreEqual("Research Viewed", driver.FindElement(By.LinkText("Research Viewed")).Text);
                Assert.AreEqual("Visited Deal Files", driver.FindElement(By.LinkText("Visited Deal Files")).Text);
                Assert.AreEqual("Visited Posts In Market Pulse", driver.FindElement(By.LinkText("Visited Posts In Market Pulse")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.LinkText("Total*")).Text, "^Total[\\s\\S]*$"));

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Subscription form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[3]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                while (IsElementVisible(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rep_AsyncWait_Wait']/table/tbody/tr/td[2]/span")))) ;
                var body = driver.FindElement(By.TagName("body"));
                String bodyText = body.Text;
                Assert.AreEqual("Client Subscriptions", driver.Title);
                Assert.AreEqual("Company:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr[2]/td/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Next", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl03")).Text);
                Assert.AreEqual("Find", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl01")).Text);
                Assert.AreEqual("of", driver.FindElement(By.XPath("//div[@id='ctl00_MC_rep_ctl06']/div/div/table/tbody/tr/td[7]/span")).Text);
                verificationTextOnThePage("Company Name", bodyText);
                verificationTextOnThePage("Client Name", bodyText);
                verificationTextOnThePage("MN / TN / SR email", bodyText);
                verificationTextOnThePage("MN EU", bodyText);
                verificationTextOnThePage("MN US", bodyText);
                verificationTextOnThePage("MN AP", bodyText);
                verificationTextOnThePage("TN EU", bodyText);
                verificationTextOnThePage("TN US", bodyText);
                verificationTextOnThePage("TN AP", bodyText);
                verificationTextOnThePage("SE EU", bodyText);
                verificationTextOnThePage("SE US", bodyText);
                verificationTextOnThePage("Extranet Access EU", bodyText);
                verificationTextOnThePage("Expiration Date", bodyText);
                verificationTextOnThePage("Expiration Date", bodyText);
                verificationTextOnThePage("Last Extranet Access", bodyText);
                verificationTextOnThePage("Last Extranet Access", bodyText);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client Box Historical Changes Report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[4]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client Box Historical Changes Report", driver.Title);
                Assert.AreEqual("Client Box Historical Changes Report", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/table/tbody/tr/td/strong")).Text);
                Assert.AreEqual("Filter changes", driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_T']/em")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr/td")).Text, "Change From:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr[2]/td")).Text, "^Change To:"));
                Assert.AreEqual("By Company Field:\r\n     --All Fields--\r\n     Address\r\n     Bigdough Client\r\n     Bloomberg Customer\r\n     City\r\n     Comments\r\n     Company Name\r\n     Company Region\r\n     Country\r\n     Date Created\r\n     Founders\r\n     Fund Classification\r\n     Funds under management\r\n     KYC Form\r\n     Opened with CC\r\n     Operations\r\n     Postcode/ZIP\r\n     Short Name\r\n     State\r\n     Status\r\n     Website\r\n     Who Created\r\n    ", driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr/td[2]")).Text);
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr[2]/td[2]")).Text, "By Client Field:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr/td[3]")).Text, "By company:"));
                Assert.IsTrue(Regex.IsMatch(driver.FindElement(By.XPath("//div[@id='ctl00_MC_rdFilter_C']/table/tbody/tr[2]/td[3]")).Text, "By client:"));
                Assert.AreEqual("Company Name", driver.FindElement(By.XPath("//table[@id='ctl00_MC_rgChanges_ctl00']/thead/tr[2]/th[2]")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.XPath("//table[@id='ctl00_MC_rgChanges_ctl00']/thead/tr[2]/th[3]")).Text);
                Assert.AreEqual("Who", driver.FindElement(By.XPath("//table[@id='ctl00_MC_rgChanges_ctl00']/thead/tr[2]/th[4]")).Text);
                Assert.AreEqual("When", driver.FindElement(By.XPath("//span[@onclick=\"javascript:__doPostBack('ctl00$MC$rgChanges$ctl00$ctl02$ctl02$ctl01','')\"]")).Text);
                Assert.AreEqual("Refresh", driver.FindElement(By.Id("ctl00_MC_rgChanges_ctl00_ctl02_ctl00_RebindGridButton")).Text);
                Assert.AreEqual("Refresh", driver.FindElement(By.Id("ctl00_MC_rgChanges_ctl00_ctl03_ctl01_RebindGridButton")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Indicative Daily Sales PNL form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[5]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Indicative Daily Sales PNL", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Sales:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div/span")).Text);
                Assert.AreEqual("Type:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[8]/span")).Text);
                Assert.AreEqual("Price:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[15]/span")).Text);
                Assert.AreEqual("Total Pnl:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[22]/span")).Text);
                Assert.AreEqual("Biz:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[2]/span")).Text);
                Assert.AreEqual("Dir:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[9]/span")).Text);
                Assert.AreEqual("Notional:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[16]/span")).Text);
                Assert.AreEqual("Prod:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[3]/span")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[10]/span")).Text);
                Assert.AreEqual("Comm:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[17]/span")).Text);
                Assert.AreEqual("Order Id:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[4]")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[11]/span")).Text);
                Assert.AreEqual("Brokerage:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[18]/span")).Text);
                Assert.AreEqual("TD:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[5]/label")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[12]/span")).Text);
                Assert.AreEqual("Tkt Chg:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[19]/span")).Text);
                Assert.AreEqual("to", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[5]/div")).Text);
                Assert.AreEqual("SD:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[6]/label")).Text);
                Assert.AreEqual("Qty:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[13]/span")).Text);
                Assert.AreEqual("Rbt:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[20]/span")).Text);
                Assert.AreEqual("to", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[6]/div")).Text);
                Assert.AreEqual("MKT:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[7]/span")).Text);
                Assert.AreEqual("CCY:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[14]/span")).Text);
                Assert.AreEqual("FIX:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[21]/span")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Delete Indicative Daily Sales PNL", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Indicative Daily Sales PNL", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Final Month End Sales PNL form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[6]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Final Month End Sales PNL", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Sales:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div/span")).Text);
                Assert.AreEqual("Type:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[8]/span")).Text);
                Assert.AreEqual("Price:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[15]/span")).Text);
                Assert.AreEqual("Total Pnl:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[22]/span")).Text);
                Assert.AreEqual("Biz:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[2]/span")).Text);
                Assert.AreEqual("Dir:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[9]/span")).Text);
                Assert.AreEqual("Notional:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[16]/span")).Text);
                Assert.AreEqual("Prod:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[3]/span")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[10]/span")).Text);
                Assert.AreEqual("Comm:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[17]/span")).Text);
                Assert.AreEqual("Order Id:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[4]/span")).Text);
                Assert.AreEqual("Client:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[11]/span")).Text);
                Assert.AreEqual("Brokerage:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[18]/span")).Text);
                Assert.AreEqual("TD:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[5]/label")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[12]/span")).Text);
                Assert.AreEqual("Tkt Chg:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[19]/span")).Text);
                Assert.AreEqual("to", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[5]/div")).Text);
                Assert.AreEqual("SD:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[6]/label")).Text);
                Assert.AreEqual("Qty:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[13]")).Text);
                Assert.AreEqual("Rbt:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[20]/span")).Text);
                Assert.AreEqual("to", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[6]/div")).Text);
                Assert.AreEqual("MKT:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[7]/span")).Text);
                Assert.AreEqual("CCY:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[14]")).Text);
                Assert.AreEqual("FIX:", driver.FindElement(By.XPath("//div[@id='ListFilters']/div[21]/span")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Delete Final Month End Sales PNL", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td[2]")).Text);
                Assert.AreEqual("Edit Final Month End Sales PNL", driver.FindElement(By.XPath("//table[@id='ctl00_MC_legendUser']/tbody/tr/td")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Indicative Daily Sales by Client form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[7]/a/span");
            try
            {
                Thread.Sleep(1000);
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Indicative Daily Sales by Client", driver.Title);
                Assert.AreEqual("INDICATIVE DAILY SALES BY CLIENT", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Date from:", driver.FindElement(By.XPath("//div[@id='filterHost']/table/tbody/tr/td/span")).Text);
                Assert.AreEqual("Date to:", driver.FindElement(By.XPath("//div[@id='filterHost']/table/tbody/tr/td[2]/span")).Text);
                Assert.AreEqual("Yesterday", driver.FindElement(By.XPath("//div[@id='filterHost']/table/tbody/tr/td[3]/span")).Text);
                Assert.AreEqual("Curr. Month", driver.FindElement(By.XPath("//div[@id='filterHost']/table/tbody/tr/td[3]/span[2]")).Text);
                Assert.AreEqual("Prev. Month", driver.FindElement(By.XPath("//div[@id='filterHost']/table/tbody/tr/td[3]/span[3]")).Text);
                Assert.AreEqual("Next", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl03")).Text);
                Assert.AreEqual("Find", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl01")).Text);
                var body = driver.FindElement(By.TagName("body"));
                String bodyText = body.Text;
                verificationTextOnThePage("Trade Date", bodyText);
                verificationTextOnThePage("Daily Total", bodyText);
                verificationTextOnThePage("Grand Total", bodyText);
                verificationTextOnThePage("Sum of $ Comm by Clients", bodyText);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Client by Month Sales PNL form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[8]/div/ul/li[8]/a/span");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Client by Month Sales PNL", driver.Title);
                Assert.AreEqual("Year:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td")).Text);
                Assert.AreEqual("User:  ", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[4]")).Text);
                Assert.AreEqual("Next", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl03")).Text);
                Assert.AreEqual("Find", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl01")).Text);
                var body = driver.FindElement(By.TagName("body"));
                String bodyText = body.Text;
                verificationTextOnThePage("Region", bodyText);
                verificationTextOnThePage("Client", bodyText);
                verificationTextOnThePage("YTD", bodyText);
                verificationTextOnThePage("YTD %", bodyText);
                verificationTextOnThePage("ANNUALISED", bodyText);
                verificationTextOnThePage("NET REVENUE", bodyText);
                verificationTextOnThePage("Net Revenue (USD) by Month for", bodyText);
                verificationTextOnThePage("Jan ", bodyText);
                verificationTextOnThePage("Feb ", bodyText);
                verificationTextOnThePage("Mar ", bodyText);
                verificationTextOnThePage("Apr ", bodyText);
                verificationTextOnThePage("May ", bodyText);
                verificationTextOnThePage("Jun ", bodyText);
                verificationTextOnThePage("Jul ", bodyText);
                verificationTextOnThePage("Aug ", bodyText);
                verificationTextOnThePage("Sep ", bodyText);
                verificationTextOnThePage("Oct ", bodyText);
                verificationTextOnThePage("Nov ", bodyText);
                verificationTextOnThePage("Dec ", bodyText);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_10_AdministrationTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("ADMINISTRATION TAB");
            currentFormOfChecking = "Administration form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/div/ul/li/a");
            try
            {
                Thread.Sleep(1000);
                checkOnError(currentFormOfChecking);
                WaitForElement(By.XPath("//div[@id='ctl00_P_pnlEdit']/table/tbody/tr/td"));
                Assert.AreEqual("Administration", driver.Title);
                Assert.AreEqual("User Name:", driver.FindElement(By.XPath("//div[@id='ctl00_P_pnlEdit']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Password:Reset", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Disabled'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Disabled'])[1]/following::label[1]")).Text);
                Assert.AreEqual("First Name:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Change Idea Status'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Last Name:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Client Box Access (0)'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Position:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User already exists'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Email:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[2]/following::td[1]")).Text);
                Assert.AreEqual("Email2:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/following::td[1]")).Text);
                Assert.AreEqual("Phone:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[5]/following::td[1]")).Text);
                Assert.AreEqual("Fax:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Position:'])[1]/following::td[2]")).Text);
                Assert.AreEqual("Mobile:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::td[1]")).Text);
                Assert.AreEqual("Region:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Email2:'])[1]/following::td[2]")).Text);
                Assert.AreEqual("Morning Note Office:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Phone:'])[1]/following::td[2]")).Text);
                Assert.AreEqual("User Role:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Fax:'])[1]/following::td[2]")).Text);
                Assert.AreEqual("Permissions:", driver.FindElement(By.Id("ctl00_P_r1")).Text);
                Assert.AreEqual("Access:", driver.FindElement(By.Id("ctl00_P_r3")).Text);
                Assert.AreEqual("Disabled", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User Role:'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Send Morning Notes", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Permissions:'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Send Strategy Research Notes", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Send Morning Notes'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Send Idea", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Send Strategy Research Notes'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Change Idea Status", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Send Idea'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Client Box Access (0)", driver.FindElement(By.Id("ctl00_P_Permissions")).Text);
                Assert.AreEqual("Add", driver.FindElement(By.Id("ctl00_F_lbAction2")).Text);
                Assert.AreEqual("Username", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Show disabled users'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Role", driver.FindElement(By.LinkText("Role")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Role'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Office", driver.FindElement(By.LinkText("Office")).Text);
                Assert.AreEqual("Region", driver.FindElement(By.LinkText("Region")).Text);
                Assert.AreEqual("Posit", driver.FindElement(By.LinkText("Posit")).Text);
                Assert.AreEqual("Email", driver.FindElement(By.LinkText("Email")).Text);
                Assert.AreEqual("DirectPhone", driver.FindElement(By.LinkText("DirectPhone")).Text);
                Assert.AreEqual("LastLoginDate", driver.FindElement(By.LinkText("LastLoginDate")).Text);
                Assert.AreEqual("Show disabled users", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add'])[1]/following::label[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            driver.Navigate().Back();

            currentFormOfChecking = "User Activity Report form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/div/ul/li[2]/a");

            try
            {
                Thread.Sleep(3000);
                checkOnError(currentFormOfChecking);
                WaitForElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Word'])[1]/following::div[12]"));
                Assert.AreEqual("Date From:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Churchillcap E-mail Web Access'])[1]/following::td[10]")).Text);
                Assert.AreEqual("Date To:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Find", driver.FindElement(By.Id("ctl00_MC_rep_ctl06_ctl03_ctl01")).Text);
                Assert.AreEqual("of", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::span[2]")).Text);
                Assert.AreEqual("Addressbook change", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Word'])[1]/following::div[12]")).Text);
                Assert.AreEqual("Announcements", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Addressbook change'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Bloomberg Update", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Announcements'])[2]/following::div[1]")).Text);
                Assert.AreEqual("Client Meeting", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Bloomberg Update'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Client modification", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Client Meeting'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Cold Call Entry", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Client modification'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Company Note/Discussion", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cold Call Entry'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Document upload", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Company Note/Discussion'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Email", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Document upload'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Event", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Focus Idea", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Focus Point", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Focus Idea'])[1]/following::div[1]")).Text);
                Assert.AreEqual("General Discussion", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Focus Point'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Idea Discussion", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='General Discussion'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Idea Modification", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Idea Discussion'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Idea Note", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Idea Modification'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Login", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Idea Note'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Morning Note item", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/following::div[1]")).Text);
                Assert.AreEqual("MorningNote", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Morning Note item'])[1]/following::div[1]")).Text);
                Assert.AreEqual("New Idea", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='MorningNote'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Research Box", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='New Idea'])[1]/following::div[1]")).Text);
                Assert.AreEqual("Research to-do", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Research Box'])[2]/following::div[1]")).Text);
                Assert.AreEqual("Web Links change", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Research to-do'])[1]/following::div[1]")).Text);
            }
            catch (Exception e)
            {
                try
                {
                    takeScreenshot(e.ToString());
                    takeScreenshot(e.ToString()); Assert.Fail("Exception: " + e.ToString());
                }
                catch (Exception e2)
                {
                    takeScreenshot(e.ToString()); Assert.Fail("Internal Exception: " + e2.ToString());
                }
            }
            addLogInformation("     Done");
            Console.WriteLine();
            //driver.Navigate().Back();


            currentFormOfChecking = "Configure History Tracking form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='User Activity Report'])[2]/following::span[1]");
            try
            {
                WaitForElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[1]/following::th[2]"));
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Configure History Tracking", driver.Title);
                Assert.AreEqual("Edit Entities", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Churchillcap E-mail Web Access'])[1]/following::span[3]")).Text);
                Assert.AreEqual("Edit Properties", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Edit Entities'])[1]/following::span[2]")).Text);
                Assert.AreEqual("Type Name", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[1]/following::th[2]")).Text);
                Assert.AreEqual("Name", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Type Name'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Enabled", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Is Knot", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Enabled'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Refresh", driver.FindElement(By.Id("ctl00_MC_ucEntities_rgEntities_ctl00_ctl02_ctl00_RebindGridButton")).Text);
                Assert.AreEqual("Change page:  1 2 ", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[2]/following::div[1]")).Text);
                ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Edit Entities'])[1]/following::span[2]"));
                Thread.Sleep(2000);
                try
                {
                    Assert.AreEqual("Field Name", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Refresh'])[3]/following::th[2]")).Text);
                    Assert.AreEqual("Name", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Field Name'])[1]/following::th[1]")).Text);
                    Assert.AreEqual("Enabled", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Name'])[2]/following::th[1]")).Text);
                }
                catch (Exception e)
                {
                    takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
                }
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            driver.Navigate().Back();

            currentFormOfChecking = "PNL permissions form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='Configure History Tracking'])[1]/following::span[1]");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("SALESPROFITANDLOSSPERMISSIONGRANTS", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Churchillcap E-mail Web Access'])[1]/following::div[3]")).Text);
                Assert.AreEqual("PNL permissions", driver.Title);
                Assert.AreEqual("Operation:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='SALESPROFITANDLOSSPERMISSIONGRANTS'])[1]/following::label[1]")).Text);
                Assert.AreEqual("Parent condition:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Operation:'])[1]/following::label[1]")).Text);
                Assert.AreEqual("UserName:", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Parent condition:'])[1]/following::span[2]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_MC_btnReset")).Text);
                Assert.AreEqual("Id", driver.FindElement(By.LinkText("Id")).Text);
                Assert.AreEqual("UserName", driver.FindElement(By.LinkText("UserName")).Text);
                Assert.AreEqual("FieldName", driver.FindElement(By.LinkText("FieldName")).Text);
                Assert.AreEqual("Operation", driver.FindElement(By.LinkText("Operation")).Text);
                Assert.AreEqual("FieldValue", driver.FindElement(By.LinkText("FieldValue")).Text);
                Assert.AreEqual("Parent condition", driver.FindElement(By.LinkText("Parent condition")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Parent condition'])[1]/following::th[1]")).Text);
                Assert.AreEqual("Edit SalesProfitAndLossPermissionGrants", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='/John Schmuckler/'])[1]/following::td[3]")).Text);
                Assert.AreEqual("Delete SalesProfitAndLossPermissionGrants", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='/John Schmuckler/'])[1]/following::td[4]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Click New Item form for a PNL permission");
            addLogInformation("     Checking titles");
            ClickCurrentElement(By.Id("ctl00_MC_InsertHyperLink"));
            try
            {
                Assert.AreEqual("Add new entry to table SalesProfitAndLossPermissionGrants", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Churchillcap E-mail Web Access'])[1]/following::h2[1]")).Text);
                Assert.AreEqual("SalesProfitAndLossPermissionGrants", driver.Title);
                Assert.AreEqual("Id", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add new entry to table SalesProfitAndLossPermissionGrants'])[1]/following::label[1]")).Text);
                Assert.AreEqual("UserName", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Id'])[1]/following::label[1]")).Text);
                Assert.AreEqual("FieldName", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::label[1]")).Text);
                Assert.AreEqual("Operation", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[6]/following::label[1]")).Text);
                Assert.AreEqual("FieldValue", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[8]/following::label[1]")).Text);
                Assert.AreEqual("Parent condition", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='*'])[11]/following::label[1]")).Text);
                Assert.AreEqual("Cancel", driver.FindElement(By.LinkText("Cancel")).Text);
                Assert.AreEqual("Insert", driver.FindElement(By.LinkText("Insert")).Text);
                Assert.AreEqual("ENTITY PROPERTIES", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/following::td[4]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();
            //driver.Navigate().Back();

            currentFormOfChecking = "Interactions form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[10]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='PNL permissions'])[1]/following::span[1]");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Interactions", driver.Title);
                Assert.AreEqual("INTERACTION HISTORICAL EXPORT", driver.FindElement(By.Id("ctl00_Header_lbAction")).Text);
                Assert.AreEqual("From", driver.FindElement(By.Id("ctl00_F_lbFrom")).Text);
                Assert.AreEqual("To", driver.FindElement(By.Id("ctl00_F_Label1")).Text);
                Assert.AreEqual("CompanyID", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='S'])[4]/following::td[49]")).Text);
                Assert.AreEqual("A2Access", driver.FindElement(By.LinkText("A2Access")).Text);
                Assert.AreEqual("Bloomberg", driver.FindElement(By.LinkText("Bloomberg")).Text);
                Assert.AreEqual("OneAccess", driver.FindElement(By.LinkText("OneAccess")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_12_HelpTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            string mainUrlPart = ConfigurationManager.AppSettings[currentVerOfMaxRav];

            addLogInformation("HELP TAB");
            currentFormOfChecking = "maxRAV Help form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/div/ul/li/a/span");

            try
            {
                driver.SwitchTo().Frame("top");
                Assert.AreEqual("maxRAV Help", driver.FindElement(By.XPath("//font")).Text);
                driver.SwitchTo().DefaultContent();
                driver.SwitchTo().Frame("main");
                driver.SwitchTo().Frame("content");
                Assert.AreEqual("Getting Started with maxRAV", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='maxRAV'])[1]/preceding::div[1]")).Text);
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "GICS Map 2008 form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='Trading FX through Bloomberg FX Portal'])[1]/following::span[1]");
            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/GICS_map2008.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Extranet Flyer CCs form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='GICS Map 2008'])[1]/following::span[1]");
            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Extranet_FlyerCCs.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "Release Notes form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='Extranet Flyer CCs'])[1]/following::span[1]");
            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Release%20Notes.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = "About maxRAV form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='Release Notes'])[1]/following::span[1]");
            try
            {
                checkOnError(currentFormOfChecking);
                Assert.AreEqual("Copyright © 2006-" + DateTime.Today.Year.ToString(), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Churchill Capital. All rights reserved.'])[1]/preceding::td[1]")).Text);
                ClickCurrentElement(By.Id("btClose"));
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / Research Manual form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)=concat('User', \"'\", 's Manuals')])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Research%20Manual.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / Sales Manual form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)='Research Manual'])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Churchill%20Sales%20Manual%20III.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / Trading Handbook form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)='Sales Manual'])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Trading%20Handbook.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / maxOMS & maxTES Handbook form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)='Trading Handbook'])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/MAX%20OMS%20and%20TES%20Handbook.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / Futures Manual form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)='maxOMS & maxTES Handbook'])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Futures%20Manual.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            currentFormOfChecking = @"Users Manuals / Trading FX through Bloomberg FX Portal form";
            addLogInformation("Check " + currentFormOfChecking);
            openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[12]/a/span")), driver,
                "(.//*[normalize-space(text()) and normalize-space(.)='maxRAV Help'])[1]/following::span[1]",
                "(.//*[normalize-space(text()) and normalize-space(.)='Futures Manual'])[1]/following::span[1]", true);

            try
            {
                string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                if (PDFTitle != mainUrlPart + "/member/UserHelp/Trading%20FX%20through%20Bloomberg%20FX%20Portal.pdf")
                    throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        [TestMethod]
        public void Case02_SmokeTest_GoThroughTabs_13_ChurchillcapEmailWebAccessTab()
        {
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("CHURCHILLCAP E-MAIL WEB ACCESS");
            currentFormOfChecking = "login.microsoftonline.com form";
            addLogInformation("Check " + currentFormOfChecking);
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[13]/a/span"));
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            ReadOnlyCollection<String> handles = null;

            try
            {
                wait.Until((d) => (handles = driver.WindowHandles).Count > 1);
                driver.SwitchTo().Window(handles[handles.IndexOf(driver.CurrentWindowHandle) ^ 1]);
                WaitForElement(By.Id("idSIButton9"));
                driver.FindElement(By.Id("idSIButton9")).Click();
                Assert.AreEqual("Enter a valid email address.", driver.FindElement(By.Id("usernameError")).Text);
                driver.Close();
                driver.SwitchTo().Window(mainWeb);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        /// <summary>
        /// Before testing make sure all required children of tree is visible on the Document Manager tab
        /// Sturcture of items for Manuals/Policies tab
        /// MAR Report + Global Restricted List + Root of Manuals and Policies + Manuals and Policies
        /// </summary>
            //[TestMethod]
        public void Case02a_SmokeTest_GoThroughManualsPoliciesAndExpenseFormsTab()
        {
            string mainUrlPart = ConfigurationManager.AppSettings[currentVerOfMaxRav];
            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            string currentFormOfChecking = String.Empty;

            // Login with proper credentials
            Case0_Login();

            addLogInformation("Getting information from Document Manager...");
            // Open the Document Manager
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[7]/div/ul/li[7]/a/span");

            List<string> listOfFolders = new List<string>();
            List<string> globalRestrictedSecurityList = new List<string>();
            List<string> manualsAndPolicies = new List<string>();
            List<KeyValuePair<string, string>> infoFromDocManager = new List<KeyValuePair<string, string>>();

            var element = driver.FindElement(By.XPath("//div[@id='ctl00_MC_uFolders_treeFoldersn3Nodes']"));
            var rows = element.FindElements(By.TagName("table"));

            foreach (var item in rows)
                listOfFolders.Add(item.Text);

            // Getting information for Manual And Policies tab
            addLogInformation("Manual And Pilicies tab current folder:");
            ClickCurrentElement(By.Id("ctl00_MC_uFolders_treeFolderst3"));
            Thread.Sleep(500);
            getInformationFromDocumentManager(manualsAndPolicies);

            manualsAndPolicies = getTitlesWithoutExtensions(manualsAndPolicies);

            foreach (var item in manualsAndPolicies)
                addLogInformation(item.ToString());
            addLogInformation("----------------------");
            //if (manualsAndPolicies.Count > 0)
            foreach (var item in manualsAndPolicies)
                infoFromDocManager.Add(new KeyValuePair<string, string>("rootManualPolicies", item));
            addLogInformation("Manual And Pilicies Folders <Folder / Title of the file>:");
            // Getting information for subfolders in Manual/Policies tab
            for (int i = 4; i < (4 + rows.Count); i++)
            {
                List<string> currentList = new List<string>();
                ClickCurrentElement(By.Id("ctl00_MC_uFolders_treeFolderst" + i.ToString()));
                Thread.Sleep(500);
                getInformationFromDocumentManager(currentList);
                currentList = getTitlesWithoutExtensions(currentList);

                foreach (var item in currentList)
                {
                    infoFromDocManager.Add(new KeyValuePair<string, string>(listOfFolders[i - 4], item));
                    addLogInformation(listOfFolders[i - 4] + " / " + item);
                }
            }
            addLogInformation("----------------------");
            // Getting infromation for Global Restricted Security List
            addLogInformation("Global Restricted Security List:");
            ClickCurrentElement(By.Id("ctl00_MC_uFolders_treeFolderst29"));
            Thread.Sleep(500);
            getInformationFromDocumentManager(globalRestrictedSecurityList);
            //if (manualsAndPolicies.Count > 0)
            foreach (var item in globalRestrictedSecurityList)
            {
                infoFromDocManager.Add(new KeyValuePair<string, string>("globalRestrictedSecurityList", item));
                addLogInformation(item);
            }

            addLogInformation("----------------------");
            addLogInformation("Checking item menu MAR Report is existed...");
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/a/span"));
            Thread.Sleep(500);

            try
            {
                Assert.AreEqual("MAR Report", driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li/a")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Item menu doesn't exist: " + e.ToString());
            }
            addLogInformation("     PASS");
            Console.WriteLine();
            // Postponed.
            // openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/a/span")), driver, "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li/a/span");

            addLogInformation("Checking Global Restricted Security List...");
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li/a"));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[2]/a"));


            for (int i = 1; i <= globalRestrictedSecurityList.Count; i++)
            {
                string currentItem = globalRestrictedSecurityList[i - 1];
                currentItem = currentItem.Replace(" ", "%20");
                string[] fileExtension = currentItem.Split('.');

                try
                {
                    var el = driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[2]/div/ul/li[" + i.ToString() + "]/a"));
                    var hr = el.GetAttribute("href");
                    string[] getDocPath = hr.Split('/');
                    string docTitle = getDocPath[getDocPath.Length - 1];
                    if (docTitle != currentItem)
                    {
                        throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.");
                    }
                    else
                    {
                        addLogInformation(docTitle.ToString() + " = " + currentItem.ToString() + ". PASS");
                    }
                }
                catch (Exception e)
                {
                    takeScreenshot(e.ToString()); Assert.Fail("Exception: " + e.ToString());
                }
                Console.WriteLine();
                #region notImplemented
                /*
                if (fileExtension[fileExtension.Length - 1] == "pdf")
                {

                    openTheFormAndSubForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li/a")), driver,
                    "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[2]/a",
                    "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[2]/div/ul/li[" + i.ToString() + "]/a", false);

                    try
                    {
                        string PDFTitle = driver.FindElement(By.XPath("//embed")).GetAttribute("src");
                        string actualTitle = mainUrlPart + "/member/files/CC/DM/Legal%20And%20Compliance/Global%20Restricted%20Securities%20List/" + currentItem;
                        if (PDFTitle != actualTitle)
                            throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.\nActual src title: " + PDFTitle.ToString());
                    }
                    catch (Exception e)
                    {
                        takeScreenshot(e.ToString()); Assert.Fail("Exception: " + e.ToString());
                    }
                    driver.Navigate().Back();
                    addLogInformation("     Done");
                    Console.WriteLine();

                    Actions actionObject = new Actions(driver);
                    actionObject.KeyDown(Keys.Control).SendKeys(Keys.F5).KeyUp(Keys.Control).Perform();
                    Thread.Sleep(1200);

                }
                */
                #endregion
            }

            addLogInformation("Checking Manual and Policies Tab..");
            addLogInformation("     Checking root files..");
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li/a"));

            List<string> actualList = new List<string>();
            foreach (var item in infoFromDocManager)
                if (item.Key == "rootManualPolicies") actualList.Add(item.Value);

            if (actualList.Count > 0)
            {
                for (int i = 3; i < (3 + manualsAndPolicies.Count); i++)
                {
                    var rootItemEl = driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[" + i.ToString() + "]/a/span")).Text;
                    if (rootItemEl != actualList[i - 3])
                    {
                        throw new ArgumentException("\nPlease check manually.\nLooks like the source document cannot be opened for some reasons.");
                    }
                }
                actualList.Clear();
            }

            for (int i = (3 + manualsAndPolicies.Count); i < (3 + manualsAndPolicies.Count + listOfFolders.Count); i++)
            {

                ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[9]/div/ul/li[" + i.ToString() + "]/a/span"));

                foreach (var item in infoFromDocManager)
                    if (item.Key == listOfFolders[i - 3 - manualsAndPolicies.Count]) actualList.Add(item.Value);

            }




            CaseLast_Logout();
        }

        /// <summary>
        /// GC implementation.
        /// Data is taken from the Excel.
        /// First line for Active Idea and second one for Archive Idea.
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\dClientDiscussions.csv", "dClientDiscussions#csv", DataAccessMethod.Sequential), DeploymentItem("dClientDiscussions.csv")]
        //[TestMethod]
        public void Case04_AddingClientDiscussion()
        {
            string mainWeb = driver.CurrentWindowHandle;

            var UserName = TestContext.DataRow["UserName"].ToString();
            var ClientName = TestContext.DataRow["ClientName"].ToString();
            var Idea = TestContext.DataRow["Idea"].ToString();
            var DiscussionType = TestContext.DataRow["DiscussionType"].ToString();
            var Duration = TestContext.DataRow["Duration"].ToString();
            var DiscussionPublic = TestContext.DataRow["DiscussionPublic"].ToString();
            var DiscussionPrivate = TestContext.DataRow["DiscussionPrivate"].ToString();

            string[] user = UserName.Split(' ');
            var userOnlyName = user[0];

            // Login with proper credentials
            Case0_Login();

            driver.Manage().Window.Maximize();
            addLogInformation("Go to the website");
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav] + "/member/Default.aspx");

            int countBefore = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            addLogInformation("Open Add Client Discussion form");
            var element = driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span"));
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/div/ul/li[1]/a/span")));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/div/ul/li[1]/a/span"));

            addLogInformation("Select Client Discussion form");
            // wait for 2 windows
            ReadOnlyCollection<String> handles = null;
            wait.Until((d) => (handles = driver.WindowHandles).Count > 1);
            // set the context on the new window
            driver.SwitchTo().Window(handles[handles.IndexOf(driver.CurrentWindowHandle) ^ 1]);

            addLogInformation("Fill Client Name fields");
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_clientSelector_tbClient_Input"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_clientSelector_tbClient_Input")).SendKeys(ClientName);
            Thread.Sleep(1000);
            ClickCurrentElement(By.XPath("//div[@id='ctl00_body_DiscussionDigest_clientSelector_tbClient_DropDown']/div/ul/li"));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ctl00_body_DiscussionDigest_clientSelector_Email")));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input")).SendKeys(Idea);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input"));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_DropDown")));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_body_DiscussionDigest_ideaSelector_tbIdea_DropDown']/div/ul/li"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"))).SelectByText(UserName);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"))).SelectByText(DiscussionType);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_duration"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_duration"))).SelectByText(Duration + " min");
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_duration"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_txtHomeTextPublic"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtHomeTextPublic")).SendKeys(DiscussionPublic);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_txtHomeText"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtHomeText")).SendKeys(DiscussionPrivate);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_btnSaveAndClose"));
            Thread.Sleep(500);
            addLogInformation("The Client discussion is added.");

            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(1200);
            WaitForElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_userHyperLink"));

            addLogInformation(" Checking if record present at \"Recent Client Discussions on our Ideas\" section.");
            var countAfter = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            if (countAfter <= countBefore)
            {
                takeScreenshot("");
                Assert.Fail("The Client Discussion is not present at \"Recent Client Discussions on our Ideas\" section");
            }

            string ideaTitle = driver.FindElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_ideaHyperLink")).Text;
            string clientTitle = driver.FindElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_clientHyperLink")).Text;

            if ((ideaTitle.Replace(" ", "") == Idea.Replace(" ", "")) && (clientTitle.Replace(" ", "") == ClientName.Replace(" ", "")))
            {
                addLogInformation("    PASS. The record present. Details: Client name = " + clientTitle + "; Idea = " + Idea + ";");
            }
            else
            {
                takeScreenshot("");
                Assert.Fail("The record was not found. Check manually.");
            }

            ClickCurrentElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_clientHyperLink"));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_MC_ClientDetailsTabs']/div/ul/li[2]/a/span"));

            addLogInformation(" Checking if record present at Client Box > Company/Client List > Client Details > Discussions tab.");
            try
            {
                Assert.AreEqual(DateTime.Today.ToString("dd-MMM-yyyy"), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[2]/following::td[1]")).Text);
                Assert.AreEqual(clientTitle, driver.FindElement(By.XPath("//div[@id='knockout-host']/div[3]/div/a")).Text);
                Assert.AreEqual(clientTitle, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[2]/following::span[2]")).Text);
                Assert.AreEqual(DiscussionPublic + " " + DiscussionPrivate, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + clientTitle + "'])[5]/following::td[1]")).Text);
                Assert.AreEqual(Idea, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[2]/following::span[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("    PASS.");
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Client Discussion'])[1]/preceding::span[1]"));
            ClickCurrentElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_ideaHyperLink"));
            addLogInformation(" Checking if record present at Client Discussion tab in Idea.");
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Address Book'])[2]/following::span[3]"));
            WaitForElement(By.XPath("//table[@id='ctl00_PH_VO7_disc_gD']/tbody/tr[2]/td/span"));
            try
            {
                Assert.AreEqual(Idea.Replace(" ", ""), driver.FindElement(By.Id("ctl00_PH_lbTickers")).Text.Replace(" ", ""));
                Assert.AreEqual(DiscussionPublic + " " + DiscussionPrivate, driver.FindElement(By.XPath("//table[@id='ctl00_PH_VO7_disc_gD']/tbody/tr[2]/td/span")).Text);
                Assert.AreEqual(DateTime.Today.ToString("dd-MMM-yyyy"), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + DiscussionPublic + " " + DiscussionPrivate + "'])[1]/following::td[1]")).Text);
                Assert.AreEqual(" " + clientTitle, driver.FindElement(By.Id("ctl00_PH_VO7_disc_gD_ctl02_clientHyperLink")).Text);
                Assert.AreEqual("Discussion", driver.FindElement(By.LinkText("Discussion")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("Company", driver.FindElement(By.LinkText("Company")).Text);
                Assert.AreEqual("User Name", driver.FindElement(By.LinkText("User Name")).Text);
                Assert.AreEqual("Actions", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='User Name'])[1]/following::th[1]")).Text);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("    PASS.");

            addLogInformation(" Checking if record present at Client Discussion tab in Client Box.");
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
               "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[8]/a/span");
            try
            {
                checkOnError("Client Discussions form");
                WaitForElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div"));
                Assert.AreEqual(DateTime.Today.ToString("dd-MMM-yyyy"), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Duration (min)'])[1]/following::td[1]")).Text);
                Assert.AreEqual(clientTitle, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_clientHyperLink")).Text);
                Assert.AreEqual(Idea.Replace(" ", ""), driver.FindElement(By.XPath("//table[@id='ctl00_MC_CDList_gvCD']/tbody/tr[2]/td[3]/a")).Text.Replace(" ", ""));
                Assert.AreEqual(DiscussionPublic + " " + DiscussionPrivate, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_Discussion")).Text);
                Assert.AreEqual(UserName, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + DiscussionPublic + " " + DiscussionPrivate + "'])[1]/following::td[1]")).Text);
                Assert.AreEqual(DiscussionType, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + UserName + "'])[1]/following::td[1]")).Text);
                Assert.AreEqual(Duration, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + DiscussionType + "'])[1]/following::td[1]")).Text);
                Assert.AreEqual("Client Discussions", driver.Title);
                Assert.AreEqual("CLIENT DISCUSSIONS", driver.FindElement(By.XPath("//tr[@id='ctl00_filterContentRow']/td/table/tbody/tr/td/div")).Text);
                Assert.AreEqual("Event:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td")).Text);
                Assert.AreEqual("Ticker:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[3]")).Text);
                Assert.AreEqual("Date Range:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[5]")).Text);
                Assert.AreEqual("Client Name:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[7]")).Text);
                Assert.AreEqual("User Name:", driver.FindElement(By.XPath("//div[@id='ctl00_F_UPF']/table/tbody/tr/td[9]")).Text);
                Assert.AreEqual("Reset", driver.FindElement(By.Id("ctl00_F_lbReset")).Text);
                Assert.AreEqual("Date", driver.FindElement(By.LinkText("Date")).Text);
                Assert.AreEqual("Client Name", driver.FindElement(By.LinkText("Client Name")).Text);
                Assert.AreEqual("Idea", driver.FindElement(By.LinkText("Idea")).Text);
                Assert.AreEqual("Discussion", driver.FindElement(By.LinkText("Discussion")).Text);
                Assert.AreEqual("User Name", driver.FindElement(By.LinkText("User Name")).Text);
                Assert.AreEqual("Discussion Type", driver.FindElement(By.LinkText("Discussion Type")).Text);
                Assert.AreEqual("Duration (min)", driver.FindElement(By.LinkText("Duration (min)")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("    PASS.");

            CaseLast_Logout();

            #region tempcode4Interactions
            /*
            // for review
            string Id = null;
            string Recipient = null;
            string RelatedEntityType = null;
            string ExceptionInfo = null;
            string Status = null;
            // add the last record from the DB
            
            addLogInformation("Select top 1 records from DB");
            var gettingItemsOfDiscussionInteraction = getSQL(@"SELECT top 1 * FROM [maxRAV-SeparatedCHURCHILLCAP-Test].[dbo].[ResourceConsumptionLog] order by [DateAdd] desc");

            foreach (var item in gettingItemsOfDiscussionInteraction)
            {
                switch (item.Key)
                {
                    case "Id":
                        Id = item.Value;                        
                        break;
                    case "Recipient":
                        Recipient = item.Value;                        
                        break;
                    case "RelatedEntityType":
                        RelatedEntityType = item.Value;
                        break;
                    case "ExceptionInfo":
                        ExceptionInfo = item.Value;
                        break;
                    case "Status":
                        Status = item.Value;
                        break;
                }
            }

            
            addLogInformation("ID: " + Id.ToString() + "; ");
            addLogInformation("Recipient: " + Recipient.ToString() + "; ");
            addLogInformation("RelatedEntityType: " + RelatedEntityType.ToString() + "; ");
            addLogInformation("ExceptionInfo: " + ExceptionInfo.ToString() + "; ");
            addLogInformation("Status: " + Status.ToString() + "; ");

            if (Recipient.ToString() == "CorpAxe" && RelatedEntityType == "Discussion")
            {
                addLogInformation("Update DateAdd for CorpAxe record Id: " + Id.ToString());
                updateDateAddForCorpAxe(Id);
            }
            
            // Push the interactions
            addLogInformation(@"URL: http://testprod.maxrav.com/ResourceConsumptionService.asmx?op=PushToCorpAxe");
            driver.Navigate().GoToUrl("http://testprod.maxrav.com/ResourceConsumptionService.asmx?op=PushToCorpAxe");
            
            ClickCurrentElement(By.XPath("//input[@value='Invoke']"));

            addLogInformation("Get Information from DB.");
            gettingItemsOfDiscussionInteraction = getSQL(@"SELECT top 1 * FROM [maxRAV-SeparatedCHURCHILLCAP-Test].[dbo].[ResourceConsumptionLog] order by [DateAdd] desc");
            */

            #endregion
        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\dIdeaNotes.csv", "dIdeaNotes#csv", DataAccessMethod.Sequential), DeploymentItem("dIdeaNotes.csv")]
        //[TestMethod]
        public void Case05_AddingIdeaNote()
        {
            string dateChecking = DateTime.Today.ToString("dd-MMM-yyyy");
            string dateCheckingv2 = DateTime.Today.ToString("dd-MMM-yy");

            var Idea = TestContext.DataRow["Idea"].ToString();
            var Rating = TestContext.DataRow["Rating"].ToString();

            var Subject = TestContext.DataRow["Subject"].ToString() + getNumberOfDayOfTheYear();
            var Content = TestContext.DataRow["Content"].ToString() + getNumberOfDayOfTheYear();
            var Source = TestContext.DataRow["Source"].ToString() + getNumberOfDayOfTheYear();

            // Login with proper credentials
            Case0_Login();

            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();

            int countBefore = driver.FindElements(By.XPath("//div[@id='ctl00_MC_companyDiscussion']/div/div[2]/div/div/table/tbody/tr")).Count;
            addLogInformation("Open Add Idea Note form");
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[2]/a/span");
            driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_tbIdea_Input")).Click();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_tbIdea_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_tbIdea_Input")).SendKeys(Idea);
            WaitForElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Idea Note'])[2]/preceding::li[1]"));
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Idea Note'])[2]/preceding::li[1]")).Click();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_tbIdea_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_selIdea_tbIdea_Input")).SendKeys(Idea);
            ClickCurrentElement(By.Id("ctl00_body_QuickNote1_tbSubject"));
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbSubject")).Clear();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbSubject")).SendKeys(Subject);
            ClickCurrentElement(By.Id("ctl00_body_QuickNote1_tbContent"));
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbContent")).Clear();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbContent")).SendKeys(Content);
            ClickCurrentElement(By.Id("ctl00_body_QuickNote1_tbSource"));
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbSource")).Clear();
            driver.FindElement(By.Id("ctl00_body_QuickNote1_tbSource")).SendKeys(Source);
            ClickCurrentElement(By.Id("ctl00_body_QuickNote1_rating_ddRating"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_QuickNote1_rating_ddRating"))).SelectByText(Rating);
            ClickCurrentElement(By.Id("ctl00_body_QuickNote1_rating_ddRating"));
            driver.FindElement(By.Id("ctl00_body_QuickNote1_btnSaveAndClose")).Click();
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(1200);

            var element = driver.FindElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_C']/div/div/table"));
            var rows = element.FindElements(By.TagName("tr"));

            int i = 0;
            int getRowInt = 0;
            bool isRecordPresentInNoteSection = false;
            List<KeyValuePair<int, string>> IdeaNotesSection = new List<KeyValuePair<int, string>>();
            foreach (var item in rows)
            {
                var tds = item.FindElements(By.TagName("td"));
                if (tds.Count == 2)
                {
                    if (tds[1].Text == dateCheckingv2)
                        IdeaNotesSection.Add(new KeyValuePair<int, string>(i, tds[0].Text));
                }
                i++;
            }

            foreach (var item in IdeaNotesSection)
            {
                if (item.Value.Contains(Subject))
                {
                    var parsedItem = item.Value.Replace(" ", "");
                    if (parsedItem.Contains(Idea.Replace(" ", "")))
                    {
                        isRecordPresentInNoteSection = true;
                        getRowInt = item.Key + 1;
                        break;
                    }
                }
            }

            addLogInformation(" Checking if record present at \"Idea Notes\" section.");
            if (!isRecordPresentInNoteSection) Assert.Fail("The record was not found in the Note Section on the Main page.");

            WaitForElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_C']/div/div/table/tbody/tr[1]/td/a"));

            var countAfter = driver.FindElements(By.XPath("//div[@id='ctl00_MC_companyDiscussion']/div/div[2]/div/div/table/tbody/tr")).Count;
            if (countAfter <= countBefore)
            {
                takeScreenshot("");
                Assert.Fail("The Client Discussion is not present at \"Idea Notes\" section");
            }

            try
            {
                Assert.AreEqual(Idea.Replace(" ", ""), driver.FindElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_C']/div/div/table/tbody/tr[" + getRowInt + "]/td/a")).Text.Replace(" ", ""));
                Assert.AreEqual(Subject, driver.FindElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_C']/div/div/table/tbody/tr[" + getRowInt + "]/td/div/a")).Text);
                Assert.AreEqual(DateTime.Today.ToString("dd-MMM-yy"), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='" + Subject + "'])[1]/following::td[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot("");
                Assert.Fail("Exception: " + e.ToString());
            }

            addLogInformation("    PASS. The record present. Details: Subject = " + Subject + "; Idea = " + Idea + ";");
            addLogInformation(" Checking if record present at \"Commentary/Developments\" tab for the Idea.");
            ClickCurrentElement(By.XPath("//div[@id='ctl00_MC_companyDiscussion_C']/div/div/table/tbody/tr[" + getRowInt + "]/td/a"));
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Summary Page'])[1]/following::span[3]"));

            List<KeyValuePair<int, string>> SubjectAtIdea = new List<KeyValuePair<int, string>>();
            List<KeyValuePair<int, string>> ContentAtIdea = new List<KeyValuePair<int, string>>();
            List<KeyValuePair<int, string>> SourceAtIdea = new List<KeyValuePair<int, string>>();

            i = 0;
            while (driver.FindElement(By.XPath("//span[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + i.ToString() + "_Literal1']")).Text == dateChecking)
            {
                SubjectAtIdea.Add(new KeyValuePair<int, string>(i, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + i.ToString() + "_tr1']/td[2]")).Text));
                ContentAtIdea.Add(new KeyValuePair<int, string>(i, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + i.ToString() + "_tr2']/td")).Text));
                SourceAtIdea.Add(new KeyValuePair<int, string>(i, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + i.ToString() + "_tr3']/td")).Text));
                i++;
            }

            bool isRecordPresent = false;
            string pItem = String.Empty;
            foreach (var item in SubjectAtIdea)
            {
                if (item.Value == Subject)
                {
                    isRecordPresent = true;
                    pItem = item.Key.ToString();
                    break;
                }
            }

            if (!isRecordPresent)
            {
                takeScreenshot("");
                Assert.Fail("The record with subject: " + Subject + " was not found!");
            }
            else
            {
                try
                {
                    Assert.AreEqual(Content, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + pItem + "_tr2']/td")).Text);
                    Assert.AreEqual(Subject, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + pItem + "_tr1']/td[2]")).Text);
                    Assert.AreEqual("Source: " + Source, driver.FindElement(By.XPath("//tr[@id='ctl00_PH_VO2_ideaHistoryList_r_ctl0" + pItem + "_tr3']/td")).Text);
                }
                catch (Exception e)
                {
                    takeScreenshot("");
                    Assert.Fail("Exception: " + e.ToString());
                }
            }

            addLogInformation("    PASS.");
            addLogInformation(" Checking if record present at \"Summary Page\" tab for the Idea.");
            ClickCurrentElement(By.Id("SummaryPageTab"));

            try
            {
                Assert.AreEqual(Subject, driver.FindElement(By.XPath("//table[@id='ctl00_PH_VO0_gNObject_C_gN']/tbody/tr[2]/td[3]/a")).Text);
                Assert.AreEqual(Source, driver.FindElement(By.XPath("//table[@id='ctl00_PH_VO0_gNObject_C_gN']/tbody/tr[2]/td[4]")).Text);
                Assert.AreEqual(dateCheckingv2, driver.FindElement(By.XPath("//table[@id='ctl00_PH_VO0_gNObject_C_gN']/tbody/tr[2]/td[5]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot("");
                Assert.Fail("Exception: " + e.ToString());
            }
            addLogInformation("    PASS.");

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case06_AddingCompanyEvent()
        {
            string dateChecking = DateTime.Today.ToString("MM/dd/yyyy");
            string ticker = "RIO LN";
            string strEvent = "Text of the Event #" + getNumberOfDayOfTheYear();

            string[] lines = dateChecking.Split('/');
            dateChecking = int.Parse(lines[0]).ToString() + "/" + int.Parse(lines[1]).ToString() + "/" + lines[2];
            // Login with proper credentials
            Case0_Login();

            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();

            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[3]/a/span");

            ClickCurrentElement(By.Id("ctl00_body_CompaneEventDiscussion1_TSelector_tbTik_Input"));
            driver.FindElement(By.Id("ctl00_body_CompaneEventDiscussion1_TSelector_tbTik_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_CompaneEventDiscussion1_TSelector_tbTik_Input")).SendKeys(ticker);
            ClickCurrentElement(By.Id("tbTik_c0"));
            Thread.Sleep(500);
            ClickCurrentElement(By.Id("ctl00_body_CompaneEventDiscussion1_DdReg"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_CompaneEventDiscussion1_DdReg"))).SelectByText("Asia-Pacific");
            ClickCurrentElement(By.Id("ctl00_body_CompaneEventDiscussion1_DdReg"));
            ClickCurrentElement(By.Id("ctl00_body_CompaneEventDiscussion1_tbEvent"));
            driver.FindElement(By.Id("ctl00_body_CompaneEventDiscussion1_tbEvent")).SendKeys(strEvent);
            driver.FindElement(By.Id("ctl00_body_CompaneEventDiscussion1_BtCancel")).Click();
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(700);
            WaitForElement(By.LinkText(ticker));

            addLogInformation(" Checking if record present at \"Upcoming Company Events:\" at the Home Page.");
            var element = driver.FindElement(By.XPath("//table[@id='ctl00_MC_companyEvents_C_wCompanyEvents_mgvCompanyEvents']"));
            var rows = element.FindElements(By.TagName("tr"));

            int getRowInt = 1;
            bool isRecordPresent = false;
            KeyValuePair<string, string> upcomingEvent;

            for (int i = 1; i < rows.Count; i++)
            {
                var tds = rows[i].FindElements(By.TagName("td"));
                if ((tds[0].Text == "0") && (tds[2].Text == ticker) && (tds[3].Text == "Text of the Event #" + getNumberOfDayOfTheYear()))
                {
                    getRowInt = i + 1;
                    upcomingEvent = new KeyValuePair<string, string>(tds[2].Text, tds[3].Text);
                    isRecordPresent = true;
                    break;
                }
            }

            if (!isRecordPresent) Assert.Fail("The event was not found in the \"Upcoming Company Events\" section.");

            try
            {
                Assert.AreEqual(dateChecking, driver.FindElement(By.XPath("//table[@id='ctl00_MC_companyEvents_C_wCompanyEvents_mgvCompanyEvents']/tbody/tr[" + getRowInt.ToString() + "]/td[2]")).Text);
                Assert.AreEqual(ticker, driver.FindElement(By.XPath("//table[@id='ctl00_MC_companyEvents_C_wCompanyEvents_mgvCompanyEvents']/tbody/tr[" + getRowInt.ToString() + "]/td[3]/a")).Text);
                Assert.AreEqual(strEvent, driver.FindElement(By.XPath("//table[@id='ctl00_MC_companyEvents_C_wCompanyEvents_mgvCompanyEvents']/tbody/tr[" + getRowInt.ToString() + "]/td[4]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot("");
                Assert.Fail("Exception: " + e.ToString());
            }
            addLogInformation("    PASS.");

            addLogInformation(" Checking if record present in all ideas with the " + ticker + " ticker.");
            ClickCurrentElement(By.Id("ac_idea"));
            driver.FindElement(By.Id("ac_idea")).Clear();
            driver.FindElement(By.Id("ac_idea")).SendKeys(ticker);
            driver.FindElement(By.Id("idea_search")).Click();
            var curCount = GettingCountOfIdeasAtPage();

            for (int i2 = 0; i2 < curCount; i2++)
            {
                var ideaStringForDisplaying = driver.FindElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a")).Text;
                var ideaString = ideaStringForDisplaying;
                ClickCurrentElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a"));
                ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='File Manager'])[1]/following::span[3]"));

                try
                {
                    WaitForElement(By.Id("ctl00_PH_VO5_cal_l_g_ctl02_ideaCompanyHyperLink"));
                    Assert.AreEqual("0", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::td[1]")).Text);
                    Assert.AreEqual(DateTime.Now.ToString("dd-MMM-yyyy"), driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[1]/following::td[2]")).Text);
                    Assert.AreEqual(ticker, driver.FindElement(By.Id("ctl00_PH_VO5_cal_l_g_ctl02_ideaCompanyHyperLink")).Text);
                    Assert.AreEqual(strEvent, driver.FindElement(By.XPath("//table[@id='ctl00_PH_VO5_cal_l_g']/tbody/tr[2]/td[4]")).Text);
                }
                catch (Exception e)
                {
                    takeScreenshot("");
                    Assert.Fail("Exception: " + e.ToString());
                }

                driver.Navigate().Back();
                Thread.Sleep(2000);
                WaitForElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a"));
            }

            addLogInformation(" Checking if record present on Company Events page on Monitors menu.");
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[5]/div/ul/li[2]/a/span");
            try
            {
                Assert.AreEqual(strEvent, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='RIO LN'])[1]/following::td[1]")).Text);
                Assert.AreEqual(ticker, driver.FindElement(By.Id("ctl00_MC_gvList_ctl02_ideaCompanyHyperLink")).Text);
                Assert.AreEqual(dateChecking, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[2]")).Text);
                Assert.AreEqual("0", driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString()); Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case07_AddClientMeetingNotes()
        {
            string mainWeb = driver.CurrentWindowHandle;
            string city = "New York";
            string duration = "30 min";
            string client = "Eugene Huang";
            string attendee = "Eugene Zolotkov";
            string ticker = "9667 JP";
            string meetingNotes = "Meeting notes #" + getNumberOfDayOfTheYear();
            string dateChecking = DateTime.Today.ToString("dd-MMM-yyyy");
            // Login with proper credentials
            Case0_Login();
            /////
            driver.Manage().Window.Maximize();
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[4]/a/span");

            ClickCurrentElement(By.Id("ctl00_body_ctl00_txtCity"));
            driver.FindElement(By.Id("ctl00_body_ctl00_txtCity")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_txtCity")).SendKeys(city);

            ClickCurrentElement(By.Id("ctl00_body_ctl00_DropDownDuration"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_ctl00_DropDownDuration"))).SelectByText(duration);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_DropDownDuration"));

            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfClients_gvList_ctl02_ClSelector_tbClient_Input"));
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfClients_gvList_ctl02_ClSelector_tbClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfClients_gvList_ctl02_ClSelector_tbClient_Input")).SendKeys(client);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfClients_gvList_ctl02_save"));

            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_UserSelector_tbUser_Input"));
            Thread.Sleep(700);
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_UserSelector_tbUser_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_UserSelector_tbUser_Input")).SendKeys(attendee);
            Thread.Sleep(700);
            ClickCurrentElement(By.Id("tbUser_c0"));
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_UserSelector_tbUser_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_UserSelector_tbUser_Input")).SendKeys(attendee);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfUsers_gvList_ctl02_save"));

            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfIdeas_gvList_ctl02_lSelector_tbIdea_Input"));
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfIdeas_gvList_ctl02_lSelector_tbIdea_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_ListOfIdeas_gvList_ctl02_lSelector_tbIdea_Input")).SendKeys(ticker);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_ListOfIdeas_gvList_ctl02_save"));
            Thread.Sleep(700);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_notes"));
            driver.FindElement(By.Id("ctl00_body_ctl00_notes")).Clear();
            driver.FindElement(By.Id("ctl00_body_ctl00_notes")).SendKeys(meetingNotes);
            ClickCurrentElement(By.Id("ctl00_body_ctl00_btnSaveAndClose"));
            Thread.Sleep(700);
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Console.WriteLine();
            addLogInformation("     Done");
            //////
            addLogInformation(" Checking if record present at Client Box Meeting tab");

            ClickCurrentElement(By.Id("ac_client"));
            driver.FindElement(By.Id("ac_client")).Clear();
            driver.FindElement(By.Id("ac_client")).SendKeys("Eugene Huang");
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Clients'])[1]/following::b[1]"));
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Discussions'])[1]/following::span[2]"));

            try
            {
                Assert.AreEqual(attendee, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='New York'])[1]/following::b[1]")).Text);
                Assert.AreEqual(meetingNotes, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Meeting Note:'])[1]/following::div[1]")).Text);
                Assert.AreEqual(attendee, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='CC attendees:'])[1]/following::td[1]")).Text);
                Assert.AreEqual(client, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Clients:'])[1]/following::span[2]")).Text);
                Assert.AreEqual(client, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Management'])[3]/following::a[7]")).Text);
                Assert.AreEqual(city, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[3]/following::b[2]")).Text);
                Assert.AreEqual(dateChecking, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Actions'])[3]/following::b[1]")).Text);
            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString());
                Assert.Fail("Exception during the login: " + e.ToString());
            }

            addLogInformation("     Done");
            Console.WriteLine();
            //addLogInformation(" Checking if record present at Client Box Meeting tab");   ??
            openTheForm(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/a/span")), driver,
                            "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[6]/div/ul/li[9]/a/span");

            try
            {
                string timestamp = driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Duration'])[1]/following::td[1]")).Text;
                string[] date = timestamp.Split(' ');
                string[] durationNumber = duration.Split(' ');
                Assert.AreEqual(dateChecking, date[0]);
                Assert.AreEqual(ticker, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_lbIdeas")).Text);
                Assert.AreEqual(client + "(Temasek Holdings)", driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_lbClientName")).Text);
                Assert.AreEqual(attendee, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_lbAttendees")).Text);
                Assert.AreEqual(city, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_Place")).Text);
                Assert.AreEqual(meetingNotes, driver.FindElement(By.Id("ctl00_MC_CDList_gvCD_ctl02_MeetingNotes")).Text);
                Assert.AreEqual(attendee, driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Meeting notes #348.1057'])[1]/following::td[1]")).Text);
                Assert.AreEqual(durationNumber[0], driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Eugene Zolotkov'])[2]/following::td[1]")).Text);

            }
            catch (Exception e)
            {
                takeScreenshot(e.ToString());
                Assert.Fail("Exception during the login: " + e.ToString());
            }
            addLogInformation("     Done");
            Console.WriteLine();

            CaseLast_Logout();
        }

        //[TestMethod]
        public void Case08_AddMultiIdeaDiscussions()
        {
            string clientName = "Dmitry Zgursky";
            string userName = "Ernez Dhondy";
            string ticker = "RIO LN|RIO AU";
            string discussionType = "Conference Call";
            string duration = "10";
            List<string> publicPrivateNotes = new List<string>();

            //Pre settings
            List<string> durations = new List<string>();
            //for (int i = 0; i < 6; i++)            
            //    durations.Add((10 + 5 * i).ToString());

            List<string> ideas = new List<string>();
            ideas.Add("ASM NA|522 HK");
            ideas.Add("UDRL US|9187891Z US");
            ideas.Add("PONR US|OLN US");
            ideas.Add("9785 JP|4837 JP");
            ideas.Add("8552 JP|8545 JP");
            ideas.Add("8853 JP|8905 JP");

            // Login with proper credentials
            Case0_Login();

            string mainWeb = driver.CurrentWindowHandle;
            driver.Manage().Window.Maximize();
            /////
            int countBefore = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            addLogInformation("Open Add Multi Idea Discussion form");
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[5]/a/span");

            ClickCurrentElement(By.Id("ctl00_body_CSelector_tbClient_Input"));
            driver.FindElement(By.Id("ctl00_body_CSelector_tbClient_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_CSelector_tbClient_Input")).SendKeys(clientName);
            ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Multi Idea Discussion'])[2]/preceding::li[1]"));
            Thread.Sleep(700);
            ClickCurrentElement(By.Id("ctl00_body_DDUsers"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DDUsers"))).SelectByText(userName);
            ClickCurrentElement(By.Id("ctl00_body_DDUsers"));

            ClickCurrentElement(By.Id("ctl00_body_DDdiscussionType"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DDdiscussionType"))).SelectByText(discussionType);
            ClickCurrentElement(By.Id("ctl00_body_DDdiscussionType"));

            driver.Manage().Window.Maximize();

            int i = 1;
            foreach (var item in ideas)
            {
                string publicNotes = "Public notes #" + getNumberOfDayOfTheYear() + " [ " + item + " ]";
                string privateNotes = "Private notes #" + getNumberOfDayOfTheYear() + " [ " + item + " ]";

                ClickCurrentElement(By.Id("ctl00_body_ISelector" + i.ToString() + "_tbIdea_Input"));
                driver.FindElement(By.Id("ctl00_body_ISelector" + i.ToString() + "_tbIdea_Input")).Clear();
                driver.FindElement(By.Id("ctl00_body_ISelector" + i.ToString() + "_tbIdea_Input")).SendKeys(item);
                WaitForElement(By.XPath("//div[@id='ctl00_body_ISelector" + i.ToString() + "_tbIdea_DropDown']/div/ul/li[1]"));
                ClickCurrentElement(By.XPath("//div[@id='ctl00_body_ISelector" + i.ToString() + "_tbIdea_DropDown']/div/ul/li[1]"));
                ClickCurrentElement(By.Id("ctl00_body_Duration" + i.ToString() + ""));
                WaitForElement(By.Id("ctl00_body_Duration" + i.ToString() + ""));
                new SelectElement(driver.FindElement(By.Id("ctl00_body_Duration" + i.ToString() + ""))).SelectByText((10 + 5 * i).ToString());
                ClickCurrentElement(By.Id("ctl00_body_Duration" + i.ToString() + ""));
                ClickCurrentElement(By.Id("ctl00_body_tbDiscussionPublic" + i.ToString() + ""));
                driver.FindElement(By.Id("ctl00_body_tbDiscussionPublic" + i.ToString() + "")).Clear();
                driver.FindElement(By.Id("ctl00_body_tbDiscussionPublic" + i.ToString() + "")).SendKeys(publicNotes);
                driver.FindElement(By.Id("ctl00_body_tbDiscussion" + i.ToString() + "")).Click();
                driver.FindElement(By.Id("ctl00_body_tbDiscussion" + i.ToString() + "")).Clear();
                driver.FindElement(By.Id("ctl00_body_tbDiscussion" + i.ToString() + "")).SendKeys(privateNotes);
                publicPrivateNotes.Add(publicNotes + " " + privateNotes);
                i++;
            }

            List<string> publicPrivateNotesResult = new List<string>();
            foreach (var item in publicPrivateNotes)
                publicPrivateNotesResult.Add(item);

            ClickCurrentElement(By.Id("ctl00_body_btAddAllClose"));
            Thread.Sleep(500);
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(1200);
            WaitForElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_userHyperLink"));
            addLogInformation("The Client discussions are added.");

            var countAfter = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            if (countAfter <= countBefore)
            {
                takeScreenshot("");
                Assert.Fail("The Client Discussions are not present at \"Recent Client Discussions on our Ideas\" section");
            }
            //////

            List<string> usersOnSection = new List<string>();
            List<string> clientsOnSection = new List<string>();
            List<string> ideasOnSection = new List<string>();

            for (int k = 0; k < 6; k++)
            {
                usersOnSection.Add(driver.FindElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl0" + k.ToString() + "_userHyperLink")).Text);
                clientsOnSection.Add(driver.FindElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl0" + k.ToString() + "_clientHyperLink")).Text);
                ideasOnSection.Add(driver.FindElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl0" + k.ToString() + "_ideaHyperLink")).Text);
            }

            addLogInformation("Checking User Names...");
            foreach (var item in usersOnSection)
            {
                if (item != userName)
                {
                    takeScreenshot("");
                    Assert.Fail("Check manually. Expected user: " + userName + "; Actual user: " + item + ";");
                }
            }

            addLogInformation("Checking Client Names...");
            foreach (var item in clientsOnSection)
            {
                if (item != clientName)
                {
                    takeScreenshot("");
                    Assert.Fail("Check manually. Expected client: " + clientName + "; Actual client: " + item + ";");
                }
            }

            addLogInformation("Checking Ideas...");
            List<string> resultIdeas = new List<string>();

            foreach (var item2 in ideas)
                resultIdeas.Add(item2.Replace(" ", ""));

            foreach (var item in ideasOnSection)
                foreach (var internalItem in ideas)
                    if (item.Replace(" ", "") == internalItem.Replace(" ", ""))
                        resultIdeas.Remove(item.Replace(" ", ""));

            if (resultIdeas.Count > 0)
            {
                string str = String.Empty;
                foreach (var item in resultIdeas)
                    str += item + "\n";
                takeScreenshot("");
                Assert.Fail("Some of the records not present. Check lines with the following ideas:\n" + str);
            }

            ClickCurrentElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_clientHyperLink"));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_MC_ClientDetailsTabs']/div/ul/li[2]/a/span"));

            addLogInformation("Checking if record present at Client Box > Company/Client List > Client Details > Discussions tab.");


            var element = driver.FindElement(By.XPath("//div[@id='ctl00_MC_Discussions']/div[2]/table/tbody[2]"));
            var rows = element.FindElements(By.TagName("tr"));

            resultIdeas.Clear();
            foreach (var item2 in ideas)
                resultIdeas.Add(item2.Replace(" ", ""));

            for (int t = 0; t < 6; t++)
            {
                var tds = rows[t].FindElements(By.TagName("td"));
                if (tds[0].Text != DateTime.Today.ToString("dd-MMM-yyyy"))
                {
                    takeScreenshot("");
                    Assert.Fail("Issue with date, Actual: " + tds[0].Text + "; Expected: " + DateTime.Today.ToString("dd-MMM-yyyy"));
                }
                if (tds[2].Text != clientName)
                {
                    takeScreenshot("");
                    Assert.Fail("Check manually. Expected client: " + clientName + "; Actual client: " + tds[2].Text + ";");
                }

                foreach (var internalItem in ideas)
                    if (tds[1].Text.Replace(" ", "") == internalItem.Replace(" ", ""))
                        resultIdeas.Remove(internalItem.Replace(" ", ""));

                foreach (var item in publicPrivateNotes)
                    if (tds[3].Text == item)
                        publicPrivateNotesResult.Remove(item);
            }

            if (publicPrivateNotesResult.Count > 0)
            {
                string str = String.Empty;
                foreach (var item in publicPrivateNotesResult)
                    str += item + "\n";
                takeScreenshot("");
                Assert.Fail("Some of the records not present. Check lines with the following Notes:\n" + str);
            }

            if (resultIdeas.Count > 0)
            {
                string str = String.Empty;
                foreach (var item in resultIdeas)
                    str += item + "\n";
                takeScreenshot("");
                Assert.Fail("Some of the records not present. Check lines with the following Notes:\n" + str);
            }

            addLogInformation("  PASS.");

            CaseLast_Logout();

        }

        //[TestMethod]
        public void Case09_AddMultiClientDiscussions()
        {
            string mainWeb = driver.CurrentWindowHandle;

            string ticker = "VOD LN|VOD US";
            string userName = "Bruno Cambuzat";
            string discussionTYpe = "Conference Call";

            List<string> publicPrivateNotes = new List<string>();
            List<string> clients = new List<string>();

            clients.Add("Dmitry Zgursky");
            clients.Add("Viktoriya Tsaritsyna");
            clients.Add("Vladimir Zdrale");
            clients.Add("Curtis Jensen");
            clients.Add("Dan Jensen");
            clients.Add("Jennifer Doyle");

            string dateChecking = DateTime.Today.ToString("dd-MMM-yyyy");

            // Login with proper credentials
            Case0_Login();

            driver.Manage().Window.Maximize();


            int countBefore = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            addLogInformation("Open Add Multi Client Discussion form");
            openTheFormInSecondWindow(driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span")), driver,
                "//div[@id='ctl00_menu_MainMenuTelerik']/ul/li/div/ul/li[6]/a/span");

            driver.Manage().Window.Maximize();

            ClickCurrentElement(By.Id("ctl00_body_ISelector_tbIdea_Input"));
            driver.FindElement(By.Id("ctl00_body_ISelector_tbIdea_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ISelector_tbIdea_Input")).SendKeys(ticker);
            ClickCurrentElement(By.XPath("//div[@id='ctl00_body_ISelector_tbIdea_DropDown']/div/ul/li"));
            driver.FindElement(By.Id("ctl00_body_ISelector_tbIdea_Input")).Clear();
            driver.FindElement(By.Id("ctl00_body_ISelector_tbIdea_Input")).SendKeys(ticker);
            Thread.Sleep(700);
            ClickCurrentElement(By.Id("ctl00_body_DDUsers"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DDUsers"))).SelectByText(userName);
            ClickCurrentElement(By.Id("ctl00_body_DDUsers"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DDdiscussionType"))).SelectByText(discussionTYpe);
            ClickCurrentElement(By.Id("ctl00_body_DDdiscussionType"));

            int j = 1;
            foreach (var item in clients)
            {
                string publicNotes = "Public notes (MCD) #" + getNumberOfDayOfTheYear() + " [ " + item + " ]";
                string privateNotes = "Private notes (MCD) #" + getNumberOfDayOfTheYear() + " [ " + item + " ]";

                ClickCurrentElement(By.Id("ctl00_body_ClientSelector" + j.ToString() + "_tbClient_Input"));
                driver.FindElement(By.Id("ctl00_body_ClientSelector" + j.ToString() + "_tbClient_Input")).Clear();
                driver.FindElement(By.Id("ctl00_body_ClientSelector" + j.ToString() + "_tbClient_Input")).SendKeys(item);
                ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Multi Client Discussion'])[1]/preceding::li[1]"));
                driver.FindElement(By.Id("ctl00_body_ClientSelector" + j.ToString() + "_tbClient_Input")).Clear();
                driver.FindElement(By.Id("ctl00_body_ClientSelector" + j.ToString() + "_tbClient_Input")).SendKeys(item);
                Thread.Sleep(900);
                ClickCurrentElement(By.Id("ctl00_body_Duration" + j.ToString() + ""));
                new SelectElement(driver.FindElement(By.Id("ctl00_body_Duration" + j.ToString() + ""))).SelectByText((10 + 5 * j).ToString());
                ClickCurrentElement(By.Id("ctl00_body_Duration" + j.ToString() + ""));
                ClickCurrentElement(By.Id("ctl00_body_tbDiscussionPublic" + j.ToString() + ""));
                driver.FindElement(By.Id("ctl00_body_tbDiscussionPublic" + j.ToString() + "")).Clear();
                driver.FindElement(By.Id("ctl00_body_tbDiscussionPublic" + j.ToString() + "")).SendKeys(publicNotes);
                ClickCurrentElement(By.Id("ctl00_body_tbDiscussion" + j.ToString() + ""));
                driver.FindElement(By.Id("ctl00_body_tbDiscussion" + j.ToString() + "")).Clear();
                driver.FindElement(By.Id("ctl00_body_tbDiscussion" + j.ToString() + "")).SendKeys(privateNotes);
                publicPrivateNotes.Add(publicNotes + " " + privateNotes);
                j++;
            }

            List<string> publicPrivateNotesResult = new List<string>();
            foreach (var item in publicPrivateNotes)
                publicPrivateNotesResult.Add(item);

            ClickCurrentElement(By.Id("ctl00_body_btAddAllClose"));
            Thread.Sleep(500);
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(1200);
            WaitForElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl00_userHyperLink"));
            addLogInformation("The Client discussions are added.");

            var countAfter = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            if (countAfter <= countBefore)
            {
                takeScreenshot("");
                Assert.Fail("The Client Discussions are not present at \"Recent Client Discussions on our Ideas\" section");
            }




            addLogInformation("Checking if record present at Client Box > Company/Client List > Client Details > Discussions tab.");

            List<string> clientsViceVersa = new List<string>();
            List<string> publicPrivateNotesViceVersa = new List<string>();

            for (int i = clients.Count - 1; i >= 0; i--)
            {
                clientsViceVersa.Add(clients[i]);
                publicPrivateNotesViceVersa.Add(publicPrivateNotes[i]);
            }

            for (int i = 0; i < clients.Count; i++)
            {
                ClickCurrentElement(By.Id("ctl00_MC_recentClientDiscussions_C_wRecentClientDiscussions_rClientDiscussion_ctl0" + i.ToString() + "_clientHyperLink"));
                ClickCurrentElement(By.XPath("//div[@id='ctl00_MC_ClientDetailsTabs']/div/ul/li[2]/a/span"));

                var element = driver.FindElement(By.XPath("//div[@id='ctl00_MC_ClientDetailsPageView']/div[2]/div[2]/table/tbody[2]"));
                var rows = element.FindElements(By.TagName("tr"));
                var tds = rows[0].FindElements(By.TagName("td"));

                if (tds[0].Text != dateChecking)
                {
                    takeScreenshot("");
                    Assert.Fail("Unexpected date: Actual: " + tds[0].Text + "; Expected: " + dateChecking + "\n");
                }

                if (tds[1].Text != ticker)
                {
                    takeScreenshot("");
                    Assert.Fail("Unexpected ticker: Actual: " + tds[1].Text + "; Expected: " + ticker + "\n");
                }

                if (tds[2].Text != clientsViceVersa[i])
                {
                    takeScreenshot("");
                    Assert.Fail("Unexpected Client Name: Actual: " + tds[2].Text + "; Expected: " + clientsViceVersa[i] + "\n");
                }

                if (tds[3].Text != publicPrivateNotesViceVersa[i])
                {
                    takeScreenshot("");
                    Assert.Fail("Unexpected topic: Actual: " + tds[3].Text + "; Expected: " + publicPrivateNotes[i] + "\n");
                }

                ClickCurrentElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Client Discussion'])[1]/preceding::span[1]"));
            }

        }

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\ClientDiscussionListOfAdding.csv", "ClientDiscussionListOfAdding#csv", DataAccessMethod.Sequential), DeploymentItem("ClientDiscussionListOfAdding.csv")]
        //[TestMethod]
        public void Function_AddBunchOfClientDiscussions()
        {
            string dayOfTheYear = DateTime.Now.DayOfYear.ToString();
            dayOfTheYear += "." + DateTime.Now.TimeOfDay.Hours.ToString() + DateTime.Now.TimeOfDay.Minutes.ToString();
            string mainWeb = driver.CurrentWindowHandle;

            var UserName = TestContext.DataRow["UserName"].ToString();
            var ClientName = TestContext.DataRow["ClientName"].ToString();
            var Idea = TestContext.DataRow["Idea"].ToString();
            var DiscussionType = TestContext.DataRow["DiscussionType"].ToString();
            var Duration = TestContext.DataRow["Duration"].ToString();
            var DiscussionPublic = TestContext.DataRow["DiscussionPublic"].ToString() + dayOfTheYear;
            var DiscussionPrivate = TestContext.DataRow["DiscussionPrivate"].ToString() + dayOfTheYear;

            string[] user = UserName.Split(' ');
            var userOnlyName = user[0];

            // Login with proper credentials
            //Case0_Login();

            driver.Manage().Window.Maximize();
            addLogInformation("Go to the website");
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav] + "/member/Default.aspx");

            int countBefore = driver.FindElements(By.XPath("//div[@id='ctl00_MC_recentClientDiscussions_C']/div/div/table/tbody/tr")).Count;

            addLogInformation("Open Add Client Discussion form");
            var element = driver.FindElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/a/span"));
            Actions action = new Actions(driver);
            action.MoveToElement(element).Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/div/ul/li[1]/a/span")));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[1]/div/ul/li[1]/a/span"));

            addLogInformation("Select Client Discussion form");
            // wait for 2 windows
            ReadOnlyCollection<String> handles = null;
            wait.Until((d) => (handles = driver.WindowHandles).Count > 1);
            // set the context on the new window
            driver.SwitchTo().Window(handles[handles.IndexOf(driver.CurrentWindowHandle) ^ 1]);

            addLogInformation("Fill Client Name fields");
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_clientSelector_tbClient_Input"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_clientSelector_tbClient_Input")).SendKeys(ClientName);
            Thread.Sleep(1000);
            ClickCurrentElement(By.XPath("//div[@id='ctl00_body_DiscussionDigest_clientSelector_tbClient_DropDown']/div/ul/li"));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ctl00_body_DiscussionDigest_clientSelector_Email")));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input")).SendKeys(Idea);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_Input"));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("ctl00_body_DiscussionDigest_ideaSelector_tbIdea_DropDown")));
            ClickCurrentElement(By.XPath("//div[@id='ctl00_body_DiscussionDigest_ideaSelector_tbIdea_DropDown']/div/ul/li"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"))).SelectByText(UserName);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDUsers"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"))).SelectByText(DiscussionType);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_DDdiscussionType"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_duration"));
            new SelectElement(driver.FindElement(By.Id("ctl00_body_DiscussionDigest_duration"))).SelectByText(Duration + " min");
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_duration"));
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_txtHomeTextPublic"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtHomeTextPublic")).SendKeys(DiscussionPublic);
            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_txtHomeText"));
            driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtHomeText")).SendKeys(DiscussionPrivate);

            if (DiscussionType == "Conference")
            {
                ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_txtCity"));
                driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtCity")).Clear();
                driver.FindElement(By.Id("ctl00_body_DiscussionDigest_txtCity")).SendKeys("New York");
            }

            ClickCurrentElement(By.Id("ctl00_body_DiscussionDigest_btnSaveAndClose"));
            Thread.Sleep(500);
            addLogInformation("The Client discussion is added.");

            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
            Thread.Sleep(500);
            driver.Navigate().Refresh();
            Thread.Sleep(1200);

            //CaseLast_Logout();
        }

        /// <summary>
        /// Written for IE only. Can be run without log in.
        /// </summary>
        //[TestMethod]
        public void Case_OpenAllActiveIdeas()
        {
            // Login with proper credentials
            Case0_Login();

            driver.Manage().Window.Maximize();
            ////log.Info("Go to the maxRav website...");
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings[currentVerOfMaxRav] + "/member/Default.aspx");
            //log.Info("DONE");
            //log.Info("Go to the Idea Page...");
            ClickCurrentElement(By.XPath("//div[@id='ctl00_menu_MainMenuTelerik']/ul/li[3]/a/span"));

            //driverIE.FindElement(By.XPath("//table[@id='ideas-table']/thead/tr/th[5]")).Click();
            //log.Info("Unselecting all countries...");
            ClickCurrentElement(By.Id("ddtCountries_SlideImage"));
            ClickCurrentElement(By.CssSelector("#ddtCountries > li > a.treeLinkButton.uncheckWholeTree"));
            //ClickCurrentElement(By.XPath("(//input[@id=''])[11]"));
            ClickCurrentElement(By.XPath("(//input[@id=''])[11]"));
            WaitForElement(By.XPath("//table[@id='ideas-table']/tbody/tr[1]/td/a"));
            ClickCurrentElement(By.CssSelector("input.formBtn"));

            // i = 11;
            for (int i = 11; i < 95; i++)
            {
                WaitForLoading();
                var s = driver.FindElement(By.XPath("(//input[@id=''])[" + i.ToString() + "]")).GetAttribute("Title");
                //log.Info("Checking all ideas for " + s + ": ");
                bool noCountries = IsElementVisible(driver.FindElement(By.Id("knockNoMatches")));

                if (noCountries)
                {
                    //log.Info("0 ideas.");
                }
                else
                {
                    var curCount = GettingCountOfIdeasAtPage();
                    //log.Info(curCount.ToString() + " ideas.");
                    IWebElement el = null;
                    Actions oAction = null;
                    string mainWeb = driver.CurrentWindowHandle;
                    for (int i2 = 0; i2 < curCount; i2++)
                    {
                        var ideaStringForDisplaying = driver.FindElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a")).Text;
                        var ideaString = ideaStringForDisplaying;
                        //log.Info("\nOpening Idea " + (i2 + 1).ToString() + " of " + curCount + ": " + ideaStringForDisplaying);
                        //WaitForElement(By.XPath("//table[@id='ideas-table']/tbody/tr[1]/td/a"));
                        WaitForElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a"));
                        el = driver.FindElement(By.XPath("//table[@id='ideas-table']/tbody/tr[" + (i2 + 1).ToString() + "]/td/a"));
                        oAction = new Actions(driver);
                        oAction.MoveToElement(el);
                        //oAction.ContextClick(el).Build().Perform();                                               
                        oAction.ContextClick(el).SendKeys(Keys.ArrowDown).SendKeys(Keys.ArrowDown).SendKeys(Keys.Enter).Build().Perform();

                        int indexToSwitch = checkWebPagesOrder(mainWeb)[1];

                        foreach (var window in driver.WindowHandles)
                        {
                            if (!window.Equals(mainWeb))
                            {
                                driver.SwitchTo().Window(driver.WindowHandles[indexToSwitch]);
                            }
                        }

                        // This case should not happened at all, just checking point to check that.
                        if (driver.CurrentWindowHandle.ToString().Equals(mainWeb))
                        {
                            //log.Warn("Case with not proper web page just happened!");
                            //log.Warn("IDEA: " + ideaStringForDisplaying);
                            /*string currentWindow2 = driverIE.CurrentWindowHandle;
                            foreach (var window in driverIE.WindowHandles)
                            {
                                if (!window.Equals(currentWindow2))
                                {
                                    driverIE.SwitchTo().Window(driverIE.WindowHandles[0]);
                                }
                            }*/
                        }

                        driver.Manage().Window.Maximize();
                        string ideaActualString = String.Empty;
                        WaitForElement(By.Id("ctl00_PH_lbTickers"));
                        if (!WaitForElement(By.Id("ctl00_PH_lbTickers")))
                        {
                            //log.Warn("FAIL: Element is not found");
                            Console.WriteLine("FAIL: Element is not found");
                        }
                        else
                        {
                            var isVisible = IsElementVisible(driver.FindElement(By.Id("ctl00_PH_lbTickers")));
                            if (isVisible)
                            {
                                ideaActualString = driver.FindElement(By.Id("ctl00_PH_lbTickers")).Text;
                                ideaString = ideaString.Replace(" ", "");
                                ideaActualString = ideaActualString.Replace(" ", "");
                                if (ideaActualString.Equals(ideaString))
                                {

                                    string[] SelectIDs = {
                                        "ctl00_MC_ideaEdit_Industry_IndustryFilter",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_ddlNatureOfDeal",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_ddDealStatusId",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_ddAcquirorTypeId",
                                        "ctl00_MC_ideaEdit_ideaSetup_ddlStrategyId",
                                        "ctl00_MC_ideaEdit_ideaSetup_ddlSubStrategyId",
                                        "ctl00_MC_ideaEdit_ideaSetup_ddlRegionId",
                                        "ctl00_MC_ideaEdit_viewIdeaFundamentalCatalists_ideaBackgroundInformation_ddlPrimaryDealManagerId",
                                        "ctl00_MC_ideaEdit_viewIdeaFundamentalCatalists_ideaBackgroundInformation_ddlSecondaryDealManagerId"
                                    };

                                    string[] InputIDs = {
                                        "ctl00_MC_ideaEdit_viewIdeaFundamentalCatalists_ideaBackgroundInformation_tbExecSummary",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_rdpUndisturbedDate_dateInput_text",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_rdpUndisturbedDate_dateInput_text",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_calCompletionDate_dateInput_text",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_calCompletionDate_dateInput_text",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_calCompletionDate_dateInput_text",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_tbStockAmount",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_tbMMCashAmount",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_tbMMStockAmount",
                                        "ctl00_MC_ideaEdit_viewIdeaArbitrage_tbPreAnnouncementTargetPrice",
                                        "ctl00_MC_ideaEdit_viewIdeaOtherCountry_tbName",
                                        "ctl00_MC_ideaEdit_Industry_tbSubindustry_Input"
                                    };

                                    List<string> requiredSelectValuesOfFields_ListOfIDs = new List<string>();
                                    requiredSelectValuesOfFields_ListOfIDs.AddRange(SelectIDs);

                                    List<string> requiredInputValuesOfFields_ListOfIDs = new List<string>();
                                    requiredInputValuesOfFields_ListOfIDs.AddRange(InputIDs);

                                    List<string> existedRequiredSelectValues_IDs = new List<string>();
                                    List<string> existedRequiredInputValues_IDs = new List<string>();

                                    //log.Info(ideaStringForDisplaying + " is opened. PASS");
                                    // Edit and Update the Idea page

                                    ClickCurrentElement(By.CssSelector("#ctl00_PH_tbimage > img"));
                                    ClickCurrentElement(By.Id("ctl00_PH_IdeaToolBoxPanel_i0_hlEdit2"));
                                    WaitForElement(By.Id("ctl00_MC_ltrIdeaEditHeader"));

                                    // Check: if required field is present at this page
                                    foreach (var id in requiredSelectValuesOfFields_ListOfIDs)
                                    {
                                        bool result = IsElementPresent(By.Id(id));
                                        if (result)
                                        {
                                            var element = driver.FindElement(By.Id(id));
                                            var disableValue = element.GetAttribute("disabled");


                                            //"ctl00_MC_ideaEdit_viewIdeaArbitrage_tbMMCashAmount",
                                            //"ctl00_MC_ideaEdit_viewIdeaArbitrage_tbMMStockAmount"

                                            existedRequiredSelectValues_IDs.Add(id);
                                        }
                                    }
                                    foreach (var id in requiredInputValuesOfFields_ListOfIDs)
                                    {
                                        bool result = IsElementPresent(By.Id(id));
                                        if (result)
                                        {
                                            existedRequiredInputValues_IDs.Add(id);
                                        }
                                    } // End Check

                                    int counterReq = 0;
                                    ///
                                    // Get information from Input fields
                                    foreach (var id in existedRequiredInputValues_IDs)
                                    {
                                        string valueText = String.Empty;
                                        var elm = driver.FindElement(By.Id(id));
                                        IWebElement forElm = null;

                                        if (IsElementPresent(By.XPath(".//*[@for='" + id + "']")))
                                            forElm = driver.FindElement(By.XPath(".//*[@for='" + id + "']"));

                                        if (elm.Text.Length == 0)
                                            valueText = elm.GetAttribute("value");
                                        else valueText = elm.Text;

                                        if ((valueText.Length == 0) || (valueText == "-- User --") || (valueText == "-- Region --"))
                                        {
                                            if (forElm != null && forElm.Text.Length > 0)
                                                //log.Info("Required field \"" + forElm.Text + "\": = " + valueText)
                                                ;
                                            else //log.Info("Required field <No found by tag FOR> : = " + valueText)
                                                ;
                                            //log.Info("htlm tag ID: " + id);
                                            counterReq++;
                                        }
                                        ////log.Info(id + " = " + valueText + "\n");
                                    }
                                    //log.Info("Select type:");
                                    // Get information from Select fields
                                    foreach (var id in existedRequiredSelectValues_IDs)
                                    {
                                        string valueText = getTextFromSelect(id);
                                        IWebElement forElm = null;
                                        if (IsElementPresent(By.XPath(".//*[@for='" + id + "']")))
                                            forElm = driver.FindElement(By.XPath(".//*[@for='" + id + "']"));

                                        if ((valueText.Length == 0) || (valueText == "-- User --") || (valueText == "-- Region --"))
                                        {
                                            if (forElm != null && forElm.Text.Length > 0)
                                                //log.Info("Required field \"" + forElm.Text + "\": = " + valueText)
                                                ;
                                            else //log.Info("Required field <No found by tag FOR> : = " + valueText)
                                                ;
                                            //log.Info("htlm tag ID: " + id);
                                            counterReq++;
                                        }
                                    }

                                    WaitForElement(By.Id("ctl00_MC_ideaEdit_btnUpdate"));
                                    ClickCurrentElement(By.Id("ctl00_MC_ideaEdit_btnUpdate"));
                                    ClickCurrentElement(By.Id("ctl00_MC_ideaEdit_btnUpdate"));
                                    IWebElement bodyTag = driver.FindElement(By.TagName("body"));
                                    if (bodyTag.Text.Contains(" is required") || counterReq > 0)
                                    {
                                        //log.Info("Required fields are present - " + counterReq.ToString() + ".\n");
                                    }
                                    else
                                    {
                                        WaitForElement(By.Id("ctl00_PH_lbTickers"));
                                        //log.Info(ideaStringForDisplaying + " has been updated.\n");
                                    }
                                }
                                else
                                {
                                    //log.Warn("FAIL: The idea is not the same! Actual: " + ideaActualString + "; Expected: " + ideaStringForDisplaying)
                                    ;
                                }
                            }
                            else //log.Warn("FAIL: Idea Title is not visible!")
                                ;
                        }

                        List<int> indexes = checkWebPagesOrder(mainWeb);
                        driver.SwitchTo().Window(driver.WindowHandles[indexes[1]]).Close();
                        driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(mainWeb)[0]]);
                    }
                }

                //log.Info("-------------------------------------------------------");
                ClickCurrentElement(By.Id("ddtCountries_SlideImage"));
                ClickCurrentElement(By.XPath("(//input[@id=''])[" + i.ToString() + "]"));
                ClickCurrentElement(By.XPath("(//input[@id=''])[" + (i + 1).ToString() + "]"));
                ClickCurrentElement(By.CssSelector("input.formBtn"));
            }

        }

        public void verificationTextOnThePage(string _text, String _bodyText)
        {
            string actualStr = String.Empty;
            if (_bodyText.Contains(_text))
                actualStr = _bodyText.ToString();
            if (actualStr.Length == 0)
            {
                addLogInformation("Pay attention! -> " + _text + " not present");
                takeScreenshot("");
                Assert.Fail(_text + " not present");
            }
        }

        public void checkOnError(string _currentFormOfChecking)
        {
            if (checkPageOnCustomText(driver, "Requested information cannot be found"))
                addLogInformation("\n\n     WARNING! Something wrong with " + _currentFormOfChecking + ".\n     Please check it directly.");
            else if (checkPageOnCustomText(driver, "Server Error"))
            {
                addLogInformation("\n\n     WARNING! Something wrong with " + _currentFormOfChecking + ".\n     Please check it directly.");
                addLogInformation("     HTML Information on the page: \n" + checkPageOnCustomText(driver, "Server Error").ToString());
            }
            takeScreenshot("");
        }

        private bool checkPageOnCustomText(IWebDriver _webDriver, string _customText)
        {
            bool result = false;
            var body = _webDriver.FindElement(By.TagName("body"));
            if (body.Text.Contains(_customText)) result = true;
            return result;
        }

        private int countOfTextOnThePage(IWebDriver _webDriver, string _string)
        {
            var body = _webDriver.FindElement(By.TagName("body"));
            String bodyText = body.Text;

            int count = 0;
            while (bodyText.Contains(_string))
            {
                count++;
                bodyText = bodyText.Substring(bodyText.IndexOf(_string) + _string.Length);
            }
            return count;
        }

        private void openTheFormAndSubForm(IWebElement _element, IWebDriver _webDriver, string _locator1, string _locator2, bool isTabSwitched)
        {
            addLogInformation("     Go to the form");
            var element = _element;
            Actions action = new Actions(driver);
            action.MoveToElement(_element).Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_locator1)));
            var element2 = driver.FindElement(By.XPath(_locator1));
            action.MoveToElement(element2).Perform();
            element2 = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_locator2)));
            ClickCurrentElement(By.XPath(_locator2));

            if (isTabSwitched)
            {
                // Additional checking to switch between tabs for Help section (Administration credentials)
                ReadOnlyCollection<String> handles = null;
                wait.Until((d) => (handles = driver.WindowHandles).Count > 1);
                // set the context on the new window
                driver.SwitchTo().Window(handles[handles.IndexOf(driver.CurrentWindowHandle) ^ 1]);
            }

            addLogInformation("     Checking titles of the fields");
        }

        private void openTheForm(IWebElement _element, IWebDriver _webDriver, string _locator)
        {
            addLogInformation("     Go to the form");
            var element = _element;
            Actions action = new Actions(driver);
            action.MoveToElement(_element).Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            Thread.Sleep(700);
            takeScreenshot("");
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_locator)));
            ClickCurrentElement(By.XPath(_locator));
            addLogInformation("     Checking titles of the fields");
        }

        private void openTheFormInSecondWindow(IWebElement _element, IWebDriver _webDriver, string _locator)
        {
            addLogInformation("     Opening the form");
            var element = _element;
            Actions action = new Actions(_webDriver);
            action.MoveToElement(element).Perform();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            element = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(_locator)));
            ClickCurrentElement(By.XPath(_locator));
            addLogInformation("     Focusing on the form");
            // wait for 2 windows
            ReadOnlyCollection<String> handles = null;
            wait.Until((d) => (handles = driver.WindowHandles).Count > 1);
            // set the context on the new window
            driver.SwitchTo().Window(handles[handles.IndexOf(driver.CurrentWindowHandle) ^ 1]);
            addLogInformation("     Checking titles of the fields");
        }

        private void closeTheForm(string _mainWeb)
        {
            addLogInformation("     Done");
            List<int> indexes = checkWebPagesOrder(_mainWeb);
            driver.SwitchTo().Window(driver.WindowHandles[indexes[1]]).Close();
            driver.SwitchTo().Window(driver.WindowHandles[checkWebPagesOrder(_mainWeb)[0]]);
            addLogInformation("     Closing the form");
        }

        public void updateDateAddForCorpAxe(string _id)
        {
            string tmpStringDateAdd = DateTime.Now.AddHours(-3).ToString("yyyy-MM-dd HH:mm:ss.fff");

            SqlConnection cnn;
            string connetionString = ConfigurationManager.ConnectionStrings["MaxRavData"].ToString();
            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                Console.WriteLine("Connection Open!\n");

                using (SqlCommand command = cnn.CreateCommand())
                {
                    command.CommandText = "Update [maxRAV-SeparatedCHURCHILLCAP-Test].[dbo].[ResourceConsumptionLog] set [DateAdd] = @DateAdd where Id = @Id";
                    command.Parameters.AddWithValue("@DateAdd", tmpStringDateAdd);
                    command.Parameters.AddWithValue("@Id", _id);
                    var result = command.ExecuteNonQuery();
                    Console.WriteLine("Result of updating" + result.ToString());
                }

                cnn.Close();
                Console.WriteLine("Connection Closed.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Can not open connection!");
                takeScreenshot("");
                Assert.Fail("\nDB issue: cannot open the connection to DB!\nThe test cannot be continue to run\nProbably VPN is not working!\nMake sure VPN is ON!");
            }
        }

        public List<KeyValuePair<string, string>> getSQL(string _command)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            SqlConnection cnn;
            string connetionString = ConfigurationManager.ConnectionStrings["MaxRavData"].ToString();
            cnn = new SqlConnection(connetionString);
            try
            {
                cnn.Open();
                Console.WriteLine("Connection Open!\n");

                using (SqlCommand command = new SqlCommand(_command, cnn))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        list.Add(new KeyValuePair<string, string>("Id", reader["Id"].ToString()));
                        list.Add(new KeyValuePair<string, string>("Recipient", reader["Recipient"].ToString()));
                        list.Add(new KeyValuePair<string, string>("RelatedEntityType", reader["RelatedEntityType"].ToString()));
                        list.Add(new KeyValuePair<string, string>("Body", reader["Body"].ToString()));

                    }
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Can not open connection!");
                takeScreenshot("");
                Assert.Fail("\nDB issue: cannot open the connection to DB!\nThe test cannot be continue to run\nProbably VPN is not working!\nMake sure VPN is ON!");
            }

            return list;

        }

        //public string getValueFormListOfKeyValuePair(string _id)        

        #region InternalFunctions

        public static List<string> getTitlesWithoutExtensions(List<string> _list)
        {
            List<string> resultList = new List<string>();

            foreach (var item in _list)
            {
                string[] line = item.Split('.');
                resultList.Add(line[0]);
            }

            return resultList;
        }

        public static List<string> getInformationFromDocumentManager(List<string> _actualList)
        {
            var element = driver.FindElement(By.XPath("//table[@id='ctl00_MC_uFiles_dgFiles']/tbody"));
            var rows = element.FindElements(By.TagName("tr"));

            foreach (var item in rows)
            {
                var tds = item.FindElements(By.TagName("td"));

                if (tds.Count > 0)
                    if (tds[1].Text != "<no files in folder>")
                    {
                        if (tds[1].Text.Length > 1)
                            _actualList.Add(tds[1].Text);
                    }
                    else break;
            }
            return _actualList;
        }

        public static string getNumberOfDayOfTheYear()
        {
            string dayOfTheYear = DateTime.Now.DayOfYear.ToString();
            dayOfTheYear += "." + DateTime.Now.TimeOfDay.Hours.ToString() + DateTime.Now.TimeOfDay.Minutes.ToString();
            return dayOfTheYear;
        }

        public static void takeScreenshot(string e_)
        {
            var timestamp = DateTime.Now.ToShortDateString() + "_" + DateTime.Now.ToShortTimeString();
            timestamp = timestamp.Replace(":", "_");// + e_.ToString();
            timestamp = timestamp.Replace("/", "_");
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile(@"\Temp\Screenshot_" + timestamp + ".jpg", ScreenshotImageFormat.Jpeg);
        }

        private void addLogInformation(string _text)
        {
            ////log.Info(_text);
            Console.WriteLine(_text);
        }

        public static string getTextFromSelect(string id_of_element)
        {
            var selected = driver.FindElement(By.Id(id_of_element));
            SelectElement selectedValue = new SelectElement(selected);
            return selectedValue.SelectedOption.Text;
        }

        public static bool IfTextPresentAtPage(IWebDriver driver, string text)
        {
            IWebElement bodyTag = UnitTest1.driver.FindElement(By.TagName("body"));
            if (bodyTag.Text.Contains(text))
                return true;
            else return false;

        }

        public static bool WaitUntilElementIsPresent(IWebDriver driver, By by, int timeout = 10)
        {
            for (var i = 0; i < timeout; i++)
            {
                if (IsElementPresent(driver, by))
                    return true;
                Thread.Sleep(500);
            }
            takeScreenshot("");
            Assert.Fail("Element <" + by.ToString() + "> is not found.");
            return false;

        }

        public void ActionFunction(string elementTag, string text, IWebDriver driver, string action)
        {
            if (IsElementPresent(driver, By.Id(elementTag)))
                if (action == "SendKeys")
                {
                    UnitTest1.driver.FindElement(By.Id(elementTag)).SendKeys(text);
                }
                else if (action == "Submit")
                {
                    UnitTest1.driver.FindElement(By.Id(elementTag)).Submit();
                }
                else if (action == "Click")
                {
                    UnitTest1.driver.FindElement(By.Id(elementTag)).Click();
                }

                else
                {
                    takeScreenshot("");
                    Assert.Fail("\nThe required <" + elementTag + "> element is not found");
                }

        }

        public static bool IsElementPresent(IWebDriver driver, By by)
        {
            try
            {
                UnitTest1.driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public List<int> checkWebPagesOrder(string mainWeb)
        {
            List<int> indexes = new List<int>(); // 1. - Index of main page, 2. - Index of subpage
            List<KeyValuePair<int, string>> webpages = new List<KeyValuePair<int, string>>();
            int index = 0;
            foreach (var window in driver.WindowHandles)
            {
                webpages.Add(new KeyValuePair<int, string>(index, window));
                index++;
            }

            int mainIndex = 0;
            int subIndex = 0;
            foreach (var item in webpages)
            {
                if (item.Value.Equals(mainWeb))
                    mainIndex = item.Key;
                else
                {
                    subIndex = item.Key;
                }
            }

            indexes.Add(mainIndex);
            if (webpages.Count > 1)
                indexes.Add(subIndex);

            return indexes;
        }

        public void ClickCurrentElement(By by)
        {
            WaitForElement(by);
            driver.FindElement(by).Click();
        }

        public void WaitForLoading()
        {
            var knockLoading = driver.FindElement(By.Id("knockLoading"));
            var s = 0;
            while (IsElementVisible(knockLoading))
            {
                Thread.Sleep(100);
                s++;
            }
            Thread.Sleep(500);
        }

        public int GettingCountOfIdeasAtPage()
        {
            int previousValue = -1;
            int rowCount = driver.FindElements(By.XPath("//table[@id='ideas-table']/tbody/tr")).Count;
            var element = driver.FindElement(By.ClassName("tit2"));
            //var element = driver.FindElement(By.LinkText(@"MaxRAV ver"));
            WaitForLoading();
            while (rowCount != previousValue)
            {
                previousValue = rowCount;
                Actions actions = new Actions(driver);
                actions.MoveToElement(element);
                actions.Perform();
                WaitForLoading();
                rowCount = driver.FindElements(By.XPath("//table[@id='ideas-table']/tbody/tr")).Count;
            }

            var elementUp = driver.FindElement(By.Id("ctl00_Header_ltTitle"));
            Actions actionsUp = new Actions(driver);
            actionsUp.MoveToElement(elementUp);
            actionsUp.Perform();

            return rowCount;
        }

        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed && element.Enabled;
        }

        /*
        private bool WaitElementIsHidden( By by)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(by));
            return false;
        }*/

        private bool WaitForElement(By by)
        {
            int secondsTimeOut = 50;
            //Console.WriteLine("[Trying to find the element " + by.ToString() +"]");
            for (int second = 0; ; second++)
            {
                if (second >= secondsTimeOut)
                {
                    takeScreenshot("");
                    Assert.Fail("[ !!! Timeout occurs: Current timeout in 0.5 second(s) is " + secondsTimeOut.ToString() + "]");
                }
                try
                {
                    if (IsElementPresent(by))
                    {
                        if (IsElementVisible(driver.FindElement(by)))
                            //Console.WriteLine("[Element exists, detection times: <= " + second.ToString() + " second(s).]\n");
                            return true;
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("[ !!! Exception: " + e.ToString() + ";\n Element " + by.ToString() + " was not found.]");
                    Console.WriteLine("[Current timeout in 500 millisecond(s): " + secondsTimeOut.ToString() + "]");
                    return false;
                }
                Thread.Sleep(500);
            }
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        #endregion

        #region NotUsedForNow

        //[TestMethod]
        public void TestMethodGC()
        {
            /*
            driverGC.Navigate().GoToUrl("http://www.google.com");
            driverGC.FindElement(By.Id("lst-ib")).SendKeys("Selenium");
            driverGC.FindElement(By.Id("lst-ib")).SendKeys(Keys.Enter);
            driverGC.Close(); // closes browser
            driverGC.Quit(); // closes IEDriverServer process
            */
        }

        //[TestMethod]
        public void TestMethodIE()
        {


            driver.Navigate().GoToUrl("http://www.google.com");

            try
            {
                using
                    (
                    //SqlCeConnection cn = new SqlCeConnection("Data Source=nonExistingSource.sdf;")
                    SqlCeConnection cn = new SqlCeConnection(ConfigurationManager.ConnectionStrings["TestData"].ToString())
                    //SqlCeConnection cn = new SqlCeConnection(@"Data Source = D:\Projects\Automation\MaxRAV\AddingSQLServerCompact\AddingSQLServerCompact\DataDriven\TestData.sdf")
                    )
                {
                    SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM TestData", cn);
                    cn.Open();
                    SqlCeDataReader reader1 = cmd.ExecuteResultSet(ResultSetOptions.Scrollable);
                    //SqlCeDataReader reader1 = cmd.ExecuteReader();                    
                    if (reader1.HasRows)
                    {
                        while (reader1.Read())
                        {
                            var s = reader1["Login"] + " | " + reader1[1] + " | " + reader1[2] + "; ";
                            driver.FindElement(By.Id("lst-ib")).SendKeys(s);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Reading from myDB: " + ex.Message);
                return;
            }


            //driverIE.Close();
            //driverIE.Quit();


            /*driverIE.Navigate().GoToUrl("http://www.google.com");            
            driverIE.FindElement(By.Id("lst-ib")).SendKeys("Selenium");
            driverIE.Close();
            driverIE.Quit();*/
            //Data Source = D:\Projects\Automation\MaxRAV\MaxRav_Selenium\TestData.sdf

            //driverIE.Navigate().GoToUrl(ConfigurationManager.AppSettings["URL"]);
            //var userData = ExcelDataAccess.GetTestData("LoginTest");

        }

        [DataSource(@"Provider=Microsoft.SqlServerCe.Client.4.0; Data Source=D:\Projects\Automation\MaxRAV\AddingSQLServerCompact\AddingSQLServerCompact\DataDriven\TestData.sdf;", "TestData")]
        //[TestMethod()]
        public void AddIntegers_FromDataSourceTest()
        {
            string str = TestContext.DataRow["Login"].ToString();

            /*var target = new Maths();

            // Access the data  
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.IntegerMethod(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
                */
        }

        #endregion

    }
}
